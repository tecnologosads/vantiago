<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 2/23/2019
 * Time: 10:49 AM
 */

class Mensagem_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}

		function create($data){
			$this->db->insert('MENSAGENS', $data);
		}

		function read(){
		   //return $this->db->where('id_van', $id)->get('mensagens')->result();
        	return $this->db->get('MENSAGENS')->result();
		}

        function getbyid($id){
			return $this->db->where('id', $id)->get('MENSAGENS')->row();
		}

        function updatesave($id, $data){
			$this->db->where('id', $id)->update('MENSAGENS', $data);
        }

        function delete($id){
			$this->db->where('id', $id)->delete('MENSAGENS');
        }
}
