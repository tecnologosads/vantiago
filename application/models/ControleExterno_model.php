<?php


class ControleExterno_model extends CI_Model
{

    function buscarVeiculos($data)
    {
        return $this->db->query('
                SELECT
                    V.ID AS ID_VEICULO,
                    V.ID_TIPO_VEICULO,
                    V.ANO,
                    V.MODELO,
                    V.FABRICANTE,
                    V.QTD_LUGARES,
                    V.PLACA,
                    P.ID AS ID_PESSOA,
                    P.ID_TIPO_USUARIO,
                    CASE 
                        when P.RAZAO_SOCIAL is null or P.RAZAO_SOCIAL = ""  then P.NOME
                        ELSE P.RAZAO_SOCIAL
                    END AS NOME,
                    B.ID AS ID_BAIRRO,
                    B.BAIRRO,
                    VT.TURNOS_ID
                    
                FROM
                    VEICULOS V
                    
                LEFT JOIN
                    PESSOAS P ON P.ID = V.ID_PESSOA
                LEFT JOIN
                    VEICULO_ATENDE_BAIRRO VB ON VB.ID_VEICULO = V.ID
                INNER JOIN
                    TB_BAIRROS B ON B.ID = VB.ID_BAIRRO
                LEFT JOIN
                    TB_CIDADES C ON C.ID = B.ID_CIDADE
                LEFT JOIN
                    VEICULOS_TB_TURNO VT ON VT.VEICULOS_ID = V.ID
                    
                WHERE
                    B.ID = "' . $data['idBairro'] . '"
                    AND VT.TURNOS_ID = "' . $data['idTurno'] . '" 
                '

        )
            ->result();
    }

    function exibirDadosUmaVan($id)
    {
        return $this->db->query('
                SELECT
                    V.ID AS ID_VEICULO,
                    V.ID_TIPO_VEICULO,
                    V.ANO,
                    V.DESCRICAO_VEICULO,
                    V.MODELO,
                    P.TELEFONE,
                    P.EMAIL,
                    V.FABRICANTE,
                    V.QTD_LUGARES,
                    V.PLACA,
                    P.ID AS ID_PESSOA,
                    P.ID_TIPO_USUARIO,
                    VTP.TIPO,
                    CASE 
                        when P.RAZAO_SOCIAL is null or P.RAZAO_SOCIAL = ""  then P.NOME
                        ELSE P.RAZAO_SOCIAL
                    END AS NOME,
                    B.ID AS ID_BAIRRO,
                    B.BAIRRO,
                    VT.TURNOS_ID
                    
                FROM
                    VEICULOS V
                    
                LEFT JOIN
                    PESSOAS P ON P.ID = V.ID_PESSOA
                LEFT JOIN
                    VEICULO_ATENDE_BAIRRO VB ON VB.ID_VEICULO = V.ID
                INNER JOIN
                    TB_BAIRROS B ON B.ID = VB.ID_BAIRRO
                LEFT JOIN
                    TB_CIDADES C ON C.ID = B.ID_CIDADE
                LEFT JOIN
                    VEICULOS_TB_TURNO VT ON VT.VEICULOS_ID = V.ID
                
                LEFT JOIN
                    VEICULOS_TIPO VTP ON VTP.ID = V.ID_TIPO_VEICULO
                    
                WHERE
                    V.ID = "' . $id . '"
                '

        )
            ->row();

    }

    function readBairrosLatitudeLongitude($idVeiculo){
        return $this->db->query('
            SELECT
                B.ID,
                B.BAIRRO,
                B.LATITUDE,
                B.LONGITUDE
            
            FROM
                TB_BAIRROS B
            
            LEFT JOIN
                VEICULO_ATENDE_BAIRRO VAB ON B.ID = VAB.ID_BAIRRO
            
            WHERE
                ID_VEICULO = "' . $idVeiculo . '"
        
        ')->result();

    }

}