<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Veiculo_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function create($data)
    {
        $this->db->insert('VEICULOS', $data);
        return $this->db->insert_id('VEICULOS');
    }
    
    function createVeiculo($dataVeiculo){
        $this->db->insert('VEICULO_ATENDE_BAIRRO', $dataVeiculo);
    }

    
    function createVeiculoBairro($bairrosVeiculos)
    {
        $this->db->insert('VEICULOS', $bairrosVeiculos);
    }

    function createTurnoVan($data){
        $this->db->insert('VEICULOS_TB_TURNO', $data);
    }
    
    function read()
    {
        return $this->db->get('VEICULOS')->result();
    }

    function readProprietarioVeiculo()
    {
         return $this->db-> query
                        (
                         'SELECT
                                P.ID,
                                V.ID AS ID_VEICULO,
                                V.ANO,
                                V.QTD_LUGARES,
                                V.PLACA,
                                
                                CASE 
                                    when P.RAZAO_SOCIAL is null or P.RAZAO_SOCIAL = ""  then P.NOME
                                    ELSE P.RAZAO_SOCIAL
                                END AS NOME
                                
                            FROM
                                PESSOAS P
                                
                            INNER JOIN
                                VEICULOS V ON P.ID = V.ID_PESSOA'
                        )->result();
        ;
    }

    function readProprietarioVeiculoWhere($data)
    {
        return $this->db-> query
        (
            'SELECT
                    P.ID,
                    V.ANO,
                    V.QTD_LUGARES,
                    V.PLACA,
                    
                    CASE 
                        when P.RAZAO_SOCIAL is null or P.RAZAO_SOCIAL = ""  then P.NOME
                        ELSE P.RAZAO_SOCIAL
                    END AS NOME
                    
                FROM
                    PESSOAS P
                    
                INNER JOIN
                    VEICULOS V ON P.ID = V.ID_PESSOA
                    
                WHERE
                    B.ID = "' . $data['idBairro'] . '"
                    AND VT.TURNOS_ID = "' . $data['idTurno'] . '"    
                '
        )->result();

    }


    function getbyid($id)
    {
        $this->db->where('ID', $id);

        return $this->db->get('VEICULOS')->row();

    }

    function updatesave($id, $data)
    {
        $this->db->where('ID', $id);
        $this->db->update('VEICULOS', $data);
    }

    function delete($id)
    {
        $this->db->where('ID', $id);
        return $this->db->delete('VEICULOS');

    }

    function deleteRota($id)
    {
        $this->db->where('ID_VEICULO', $id);
        return $this->db->delete('VEICULO_ATENDE_BAIRRO');

    }
    
    function deleteTurnoVan($id){
        $this->db->where('VEICULOS_ID', $id);
        return $this->db->delete('VEICULOS_TB_TURNO');
        
    }

    function carregarQuatidadeVeiculos($id)
    {
        return $this->db->where('ID_PESSOA', $id)->get('VEICULOS')->result();
    }

    function readCidades(){
        return $this->db->query('SELECT
                                        ID,
                                        CIDADE
                                      FROM
                                        TB_CIDADES
                                      
                                      ORDER BY
                                        CIDADE
                                          
                                      ')
        ->result();

    }

    function readBairros(){
        return $this->db->query('SELECT
                                        BAIRRO
                                      FROM
                                        TB_BAIRROS
                                      
                                      ORDER BY
                                        BAIRRO
                                          
                                      ')
            ->result();

    }

    function readBairros2($idCidade){
        return $this->db->query('SELECT * FROM
                                        TB_BAIRROS
                                      WHERE
                                        ID_CIDADE = '.$idCidade.'
            
                                      ORDER BY
                                        BAIRRO
            
                                      ')
                                      ->result();
                                      
    }

    function qtd_veiculos($id){
    	return $this->db
			->where('ID_PESSOA', $id)
			->get('VEICULOS')
			->result();
	}

    function readCidadesEBairros(){
        return $this->db->query
            ('
                SELECT
                    B.ID AS ID_BAIRRO,
                    B.BAIRRO,
                    B.ID_CIDADE,
                    C.CIDADE,
                    C.UF
                 
                FROM
                    TB_CIDADES C
                
                LEFT JOIN
                    TB_BAIRROS B ON B.ID_CIDADE = C.ID
                    
                
                
                ORDER BY
                    
                    B.BAIRRO
              ')
       ->result();
                                      
    }

    function readCidadesEBairrosJson($idCidade){
        return $this->db->query
        ('
                SELECT DISTINCT
                    B.ID as id,
                    B.BAIRRO as nome_sub_categoria
                 
                FROM
                    TB_BAIRROS B
                
                INNER JOIN
                    VEICULO_ATENDE_BAIRRO VAB ON VAB.ID_BAIRRO = B.ID
                    
                WHERE
                    B.ID_CIDADE = "'.$idCidade.'"    
                
                ORDER BY
                    B.BAIRRO
              ')
            ->result();

    }

}
