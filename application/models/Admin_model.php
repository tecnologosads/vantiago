<?php

class Admin_model extends CI_Model
{

	public function create($data)
	{
		$this->db->insert("PESSOAS", $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('PESSOAS');
	}

	public function read()
	{
		return $this->db->where('ID_TIPO_USUARIO', '1')->get("PESSOAS")->result();
	}

	function getbyid($id)
	{
		$this->db->where('ID', $id);
		return $this->db->get("PESSOAS")->row();
	}

	function updatesave($id, $data)
	{
		$this->db->where('ID', $id);
		$this->db->update('PESSOAS', $data);
	}

	public function listarAdministradores()
	{
		$this->db->where("ID_TIPO_USUARIO", "1");
		return $this->db->get("PESSOAS")->result_array();
	}

	public function listarAdministradores2($id){
		$this->db->where("id", $id);
		return $this->db->get("PESSOAS")->result_array();
	}

	public function editarAdministradorBanco($administrador, $id){
		$this->db->where("ID", $id);
		$this->db->update("PESSOAS", $administrador);
		if ($this->db->affected_rows() >= 0) {
			return TRUE;
		}
		return FALSE;
	}
}