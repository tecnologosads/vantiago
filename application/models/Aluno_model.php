<?php

class Aluno_model extends CI_Model
{
	public function create($data)
	{
		$this->db->insert("PESSOAS", $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('PESSOAS');
	}

	public function read()
	{
		return $this->db->where('ID_TIPO_USUARIO', '7')->get("PESSOAS")->result();
	}

	function getbyid($id)
	{
		$this->db->where('ID', $id);
		return $this->db->get("PESSOAS")->row();
	}

	function updatesave($id, $data)
	{
		$this->db->where('ID', $id);
		$this->db->update('PESSOAS', $data);
	}

	public function listarAluno()
	{
		$this->db->where("ID_TIPO_USUARIO", "7");
		return $this->db->get("PESSOAS")->result_array();
	}

	public function listarAluno2($id){
		$this->db->where("id", $id);
		return $this->db->get("PESSOAS")->result_array();
	}

	public function editarAlunoBanco($administrador, $id){
		$this->db->where("ID", $id);
		$this->db->update("PESSOAS", $administrador);
		if ($this->db->affected_rows() >= 0) {
			return TRUE;
		}
		return FALSE;
	}

    function exibirVanAluno()
    {
        $idSession = $this->session->userdata('id');

        return $this ->db ->query('
                                    SELECT * FROM
                                        VEICULOS V
                                        
                                    WHERE
                                        V.ID_PESSOA = "'.$idSession.'"
                                    '
        )->result();
    }

    function listarAlunosExibir(){
        $idSession = $this->session->userdata('id');

        return $this ->db ->query('
                                        SELECT  
                                            P.ID,
                                            P.ID_TIPO_USUARIO,
                                            P.NOME,
                                            P.CPF,
                                            P.TELEFONE,
                                            P.CIDADE,
                                            P.BAIRRO,
                                            P.ENDERECO,
                                            P.EMAIL,
                                            V.MODELO,
                                            V.ANO,
                                            V.ID AS ID_VEICULO
                                            
                                        FROM
                                            PESSOAS P
                                            
                                        LEFT JOIN
                                            VEICULOS V ON V.ID = P.ID_VEICULO 
                                        
                                        WHERE
                                            V.ID_PESSOA = "'.$idSession.'"
                                            
                                        ORDER BY
                                            V.MODELO,
                                            P.NOME
                                        '
        ) ->result();

    }
    // Lista a van que o Aluno está usando;
    function listarVanAluno($idAluno){


        return $this ->db ->query('
                                        SELECT  
                                            P.ID,
                                            P.ID_TIPO_USUARIO,
                                            P.NOME,
                                            P.CPF,
                                            P.TELEFONE,
                                            P.CIDADE,
                                            P.BAIRRO,
                                            P.ENDERECO,
                                            P.EMAIL,
                                            V.ID AS ID_VEICULO,
                                            V.MODELO,
                                            V.ANO
                                            
                                        FROM
                                            PESSOAS P
                                            
                                        LEFT JOIN
                                            VEICULOS V ON V.ID = P.ID_VEICULO 
                                        
                                        WHERE
                                            P.ID = "'.$idAluno.'"
                                        
                                        '
        ) ->result();
    }


}
