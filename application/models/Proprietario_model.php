<?php

class Proprietario_model extends CI_Model
{

    public function create($data)
    {
        $this->db->insert("PESSOAS", $data);
    }

    function delete($id)
    {
        $this->db->where('ID', $id);
        return $this->db->delete('PESSOAS');
    }

    public function read()
    {
        return $this->db->where('ID_TIPO_USUARIO', '3')->get("PESSOAS")->result();
    }

    function getbyid($id)
    {
        $this->db->where('ID', $id);
        return $this->db->get("PESSOAS")->row();
    }

    function updatesave($id, $data)
    {
        $this->db->where('ID', $id);
        $this->db->update('PESSOAS', $data);
    }


}
