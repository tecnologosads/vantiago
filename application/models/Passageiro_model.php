<?php
class Passageiro_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}

		function create($data){
			$this->db->insert('PESSOAS', $data);
		}

		function read(){
			return $this->db->where('ID_TIPO_USUARIO', '6')->get('PESSOAS')->result();
        }

        function getbyid($id){
			return $this->db->where('ID', $id)->get('PESSOAS')->row();
		}

        function updatesave($id, $data){
			$this->db->where('ID', $id)->update('PESSOAS', $data);
        }

        function delete($id){
			$this->db->where('ID', $id)->delete('PESSOAS');
        }

        function carregarEstados(){
			return $this->db->get("TB_CIDADES")->result();
		}
}
