<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 2/22/2019
 * Time: 10:15 PM
 */

class Rotas_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}

		function create($data){
			$this->db->insert('ROTAS', $data);
		}
		
		function read(){
			return $this->db->get('ROTAS')->result();
        }

        function getbyid($id){
			return $this->db->where('id', $id)->get('ROTAS')->row();
		}

        function updatesave($id, $data){
			$this->db->where('id', $id)->update('ROTAS', $data);
        }

        function delete($id){
			$this->db->where('id', $id)->delete('ROTAS');
        }
        
        function readCidades(){
            return $this->db->query('SELECT * FROM
                                        TB_CIDADES
                
                                      ORDER BY
                                        CIDADE
                
                                      ')
                                      ->result();
                                      
        }
        
        function readBairros(){
            return $this->db->query('SELECT * FROM
                                        TB_BAIRROS
                
                                      ORDER BY
                                        BAIRRO
                
                                      ')
                                      ->result();
                                      
        }
        
        
        function readBairros2($idCidade){
            return $this->db->query('SELECT * FROM
                                        TB_BAIRROS
                                      WHERE
                                        ID_CIDADE = '.$idCidade.'
                
                                      ORDER BY
                                        BAIRRO
                
                                      ')
                                      ->result();
                                      
        }
        
        function readCidadesEBairros(){
            return $this->db->query
            ('
                SELECT
                    B.ID ID_BAIRRO,
                    B.BAIRRO,
                    B.ID_CIDADE,
                    C.CIDADE,
                    C.UF
                
                FROM
                    TB_CIDADES C
                
                LEFT JOIN
                    TB_BAIRROS B ON B.ID_CIDADE = C.ID
                
                ORDER BY
                    C.CIDADE,
                    B.BAIRRO
                
              ')
              ->result();
              
        }
        
        function exibirVanAluno()
        {
            $idSession = $this->session->userdata('id');
            
            return $this ->db ->query('
                                    SELECT * FROM
                                        VEICULOS V
                
                                    WHERE
                                        V.ID_PESSOA = "'.$idSession.'"
                                    '
                )->result();
        }
        
        
}
