<?php

class ControleExterno extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("ControleExterno_model");
        $this->load->model("Veiculo_model");
    }

    function index()
    {
        $this->home();
    }


    function home()
    {
        $exibirCidades = $this->Veiculo_model->readCidades();
        $this->data['cidades'] = $exibirCidades;

        $listarProprietario = $this->Veiculo_model->readProprietarioVeiculo();
        $this->data['vanDestaque'] = $listarProprietario;
        $this->load->view("sistema/home/home", $this->data);
    }

    function buscarVans()
    {
        $data = array(
            'idBairro' => $this->input->post("idBairro"),
            'idTurno' => $this->input->post("idTurno")
        );

        $lista = $this->ControleExterno_model->buscarVeiculos($data);
        $this->data['listarVans'] = $lista;

        $this->load->view("sistema/home/buscarVeiculo", $this->data);
    }

    function exibirUmaVan()
    {
        $this->data['perfils'] = $this->ControleExterno_model->exibirDadosUmaVan($this->uri->segment(3));
        $this->data['exibirLatitudeLongitude'] = $this->ControleExterno_model->readBairrosLatitudeLongitude($this->uri->segment(3));
        $this->load->view("sistema/home/exibirVan", $this->data);
    }

}