<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 3/17/2019
 * Time: 2:52 PM
 */

class Cidade extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model("Cidade_model");
		}

		function index(){
			$this->read();
		}

//Carregar estados dentro da view de passageiros
		function carregar_estados(){
//			Carregar models externas


//			Passando informações para o array
			$this->data['cidades'] = $this->Cidade_model->carregar_Estados();

//			Carregando view
        	$this->load->view("sistema/passageiro/cadastro", $this->data);
		}
}
