<?php

class Proprietario extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Proprietario_model");
		$this->load->model('Veiculo_model');
        $logado = $this->session->userdata('nome');
        if(empty($logado)){
            redirect('/');
        }
    }

    function index()
    {
        $this->read();
    }

    function loadcreate()
    {
        $this->load->view("sistema/proprietarios/create");
    }

    function create()
    {
        $data = array(
            "id_tipo_usuario" => 3,
            "nome" => $this->input->post("nome"),
            "cpf" => $this->input->post("cpf"),
            "cnpj" => $this->input->post("cnpj"),
            "razao_social" => $this->input->post("razao_social"),
            "rg" => $this->input->post("rg"),
            "data_nascimento" => $this->input->post("data_nascimento"),
            "endereco" => $this->input->post("endereco"),
            "bairro" => $this->input->post("bairro"),
            "cep" => $this->input->post("cep"),
            "cidade" => $this->input->post("cidade"),
            "telefone" => $this->input->post("telefone"),
            "telefone2" => $this->input->post("telefone2"),
            "email" => $this->input->post("email"),
            "senha" => md5($this->input->post("senha"))

        );
        $this->Proprietario_model->create($data);
        redirect('proprietario');
    }

    function read()
    {
        $lista = $this->Proprietario_model->read();
        $this->data['proprietarios'] = $lista;
        $this->load->view('sistema/proprietarios/proprietario', $this->data);
    }

    function update()
    {
        $id = $this->uri->segment(3);
        $this->data['updates'] = $this->Proprietario_model->getbyid($id);
        $this->load->view('sistema/proprietarios/update', $this->data);
    }

    function updatesave()
    {
        $data = array(
            "nome" => $this->input->post("nome"),
            "cpf" => $this->input->post("cpf"),
            "cnpj" => $this->input->post("cnpj"),
            "razao_social" => $this->input->post("razao_social"),
            "rg" => $this->input->post("rg"),
            "data_nascimento" => $this->input->post("data_nascimento"),
            "endereco" => $this->input->post("endereco"),
            "bairro" => $this->input->post("bairro"),
            "cep" => $this->input->post("cep"),
            "cidade" => $this->input->post("cidade"),
            "telefone" => $this->input->post("telefone"),
            "telefone2" => $this->input->post("telefone2"),
            "email" => $this->input->post("email"),
            "senha" => md5($this->input->post("senha"))

        );
        $this->Proprietario_model->updatesave($this->input->post('idsave'), $data);
        $this->data['updates'] = $this->Proprietario_model->getbyid($this->input->post('idsave'));
        redirect('proprietario');
    }

    function delete()
    {
        $id = $this->uri->segment(3);
        $this->proprietario_model->delete($id);
        redirect('proprietario');
    }

	function carregarPerfil(){


    	$this->data['qtd_veiculos'] = $this->Veiculo_model->qtd_veiculos($this->uri->segment(3));
		$this->data['perfils'] = $this->Proprietario_model->getbyid($this->uri->segment(3));
		$this->load->view("sistema/proprietarios/perfil", $this->data);
	}

}
