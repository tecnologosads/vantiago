<?php
defined('BASEPATH') OR exist('No direct script acess allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->view("sistema/autenticacao/login");
       
    }

    function autenticar()
    {
        $this->load->model("login_model");
        $login = $this->input->post("login");
        $senha = md5($this->input->post("senha"));
        $loginAdministrador = $this->login_model->logarAdministrador($login, $senha);

        $query = $this->login_model->validarSessao($login);

        if ($loginAdministrador) {
            $this->session->set_userdata("administrador_logado", $loginAdministrador);
            $this->session->set_flashdata("sucess", "Logado com sucesso");

            $usuario = $query;
            
            $this -> session ->set_userdata("nome", $usuario ->NOME);
            $this -> session ->set_userdata("id", $usuario ->ID);
            $this -> session ->set_userdata("cpf", $usuario ->CPF);

            redirect('Dashboard');

        } else {
            $this->session->set_flashdata("danger", "Usuário ou senhas inválidos.");
            redirect('/');
        }
    }
}
