<?php
class Dashboard extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model("Dashboard_model");
			$this->load->model('Veiculo_model');
		}

		function index(){
			$this->informacoes();
		}

		function informacoes(){
			$this->data['veiculos'] = $this->Veiculo_model->carregarQuatidadeVeiculos($this->session->userdata('id'));
			$this->data['proprietarios'] = $this->Veiculo_model->readProprietarioVeiculo($this->session->userdata('id'));
			$this->load->view("sistema/dashboard/dashboard", $this->data);
		}
}
