<?php

class Aluno extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("aluno_model");

        $logado = $this->session->userdata('nome');
            if(empty($logado)){
                redirect('/');
            }
    }

    function index(){
        $this->read();
    }

    function loadcreate(){

        $lista = $this->aluno_model->exibirVanAluno();
        $this->data['vans'] = $lista;
        $this->load->view("sistema/aluno/create", $this->data);
    }

    function create(){
        $data = array(
            "id_tipo_usuario" => 7,
            "cpf" => $cpf = $this->replaceMasc($this->input->post("cpf")),
            "nome" => $nome = $this->input->post("nome"),
            "data_nascimento" => $data_nascimento = $this->input->post("data_nascimento"),
            "id_veiculo" => $id_veiculo = $this->input->post("id_veiculo"),
            "telefone" => $telefone1 = $this->replaceMasc($this->input->post("telefone")),
            "endereco" => $endereco = $this->input->post("endereco"),
            "cidade" => $cidade = $this->input->post("cidade"),
            "bairro" => $bairro = $this->input->post("bairro"),
            "email" => $email = $this->input->post("email")
        );
        $this->salvarImagem($cpf);
        $this->aluno_model->create($data);
        redirect('aluno');
    }

    function read(){
        $lista = $this->aluno_model->listarAlunosExibir();
        $this->data['alunos'] = $lista;
        $this->load->view('sistema/aluno/aluno', $this->data );
    }

    function update(){
        $id = $this->uri->segment(3);
        $lista = $this->aluno_model->exibirVanAluno();
        $this->data['vans'] = $lista;
        $this->data['updates'] = $this->aluno_model->getbyid($id);
        $this->data['listarVanAluno'] = $this->aluno_model->listarVanAluno($id);
        $this->load->view('sistema/aluno/update', $this->data);
    }

    function updatesave(){
        $data = array(
            "cpf" => $cpf = $this->replaceMasc($this->input->post("cpf")),
            "nome" => $nome = $this->input->post("nome"),
            "data_nascimento" => $data_nascimento = $this->input->post("data_nascimento"),
            "id_veiculo" => $id_veiculo = $this->input->post("id_veiculo"),
            "telefone" => $telefone1 = $this->replaceMasc($this->input->post("telefone")),
            "endereco" => $endereco = $this->input->post("endereco"),
            "cidade" => $cidade = $this->input->post("cidade"),
            "bairro" => $bairro = $this->input->post("bairro"),
            "email" => $email = $this->input->post("email")
        );

        $this->aluno_model->updatesave($this->input->post('idsave'), $data);
        $this->data['updates'] = $this->aluno_model->getbyid($this->input->post('idsave'));
        $this->salvarImagem($cpf);

        redirect('aluno');
    }

    function delete(){
        $id = $this->uri->segment(3);
        $this->aluno_model->delete($id);
        redirect('aluno');
    }

    function replaceMasc($soNumero){
        return preg_replace("/[^0-9]/", "", $soNumero);
    }

    function salvarImagem($cpf)
    {
        $diretorio = 'assets/sistema/images/imagens_pessoas/';

        $nomeDoArquivo = $_FILES['adicionar_imagem'];

//        if (file_exists($diretorio . $cpf . '.jpg')) {
//           unlink($diretorio . $cpf . '.jpg');
//        }

        $configuracao = array(
            'upload_path' => $diretorio,
            'allowed_types' => 'jpg',
            'file_name' => $cpf . '.jpg',
            'max_size' => '2000'
        );

        $this->load->library('upload');
        $this->upload->initialize($configuracao);

        if ($this->upload->do_upload('adicionar_imagem'))
            echo 'Arquivo salvo com sucesso.';
        else
            echo $this->upload->display_errors() . $nomeDoArquivo;
    }






}
