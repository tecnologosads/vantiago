<?php

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("admin_model");

        $logado = $this->session->userdata('nome');
            if(empty($logado)){
                redirect('/');
            }
    }

    function index()
    {
        $this->read();
    }

    function loadcreate()
    {
        $this->load->view("sistema/admin/create");
    }

    function create()
    {
        $data = array(
            "id_tipo_usuario" => 1,
            "cpf" => $cpf = $this->replaceMasc($this->input->post("cpf")),
            "nome" => $nome = $this->input->post("nome"),
            "data_nascimento" => $data_nascimento = $this->input->post("data_nascimento"),
            "genero" => $genero = $this->input->post("genero"),
            "rg" => $rg = $this->input->post("rg"),
            "telefone" => $telefone1 = $this->replaceMasc($this->input->post("telefone")),
            "telefone2" => $telefone2 = $this->replaceMasc($this->input->post("telefone2")),
            "cep" => $cep = $this->replaceMasc($this->input->post("cep")),
            "endereco" => $endereco = $this->input->post("endereco"),
            "cidade" => $cidade = $this->input->post("cidade"),
            "bairro" => $bairro = $this->input->post("bairro"),
            "email" => $email = $this->input->post("email"),
            "senha" => $senha = md5($this->input->post("senha"))
        );
        $this->salvarImagem($cpf);
        $this->admin_model->create($data);
        redirect('admin');
    }

    function read()
    {
        $lista = $this->admin_model->read();
        $this->data['administradores'] = $lista;
        $this->load->view('sistema/admin/admin', $this->data);
    }

    function update()
    {
        $id = $this->uri->segment(3);
        $this->data['updates'] = $this->admin_model->getbyid($id);
        $this->load->view('sistema/admin/update', $this->data);
    }

    function updatesave()
    {
        $data = array(
            "cpf" => $cpf = $this->replaceMasc($this->input->post("cpf")),
            "nome" => $nome = $this->input->post("nome"),
            "data_nascimento" => $data_nascimento = $this->input->post("data_nascimento"),
            "genero" => $genero = $this->input->post("genero"),
            "rg" => $rg = $this->input->post("rg"),
            "telefone" => $telefone1 = $this->replaceMasc($this->input->post("telefone")),
            "telefone2" => $telefone2 = $this->replaceMasc($this->input->post("telefone2")),
            "cep" => $cep = $this->replaceMasc($this->input->post("cep")),
            "endereco" => $endereco = $this->input->post("endereco"),
            "cidade" => $cidade = $this->input->post("cidade"),
            "bairro" => $bairro = $this->input->post("bairro"),
            "email" => $email = $this->input->post("email"),
            "senha" => $senha = md5($this->input->post("senha"))
        );

        $this->admin_model->updatesave($this->input->post('idsave'), $data);
        $this->data['updates'] = $this->admin_model->getbyid($this->input->post('idsave'));
        $this->salvarImagem($cpf);

        redirect('Admin');
    }

    function delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete($id);
        redirect('admin');
    }

    function replaceMasc($soNumero)
    {
        return preg_replace("/[^0-9]/", "", $soNumero);
    }

    function salvarImagem($cpf)
    {
        $nome_imagem = $this->session->userdata('link_foto');

        $diretorio_imagem_recortada = "assets/images/img_crop/$nome_imagem";

        $destino_imagem_veiculo = "assets/sistema/images/imagens_pessoas/$cpf";

        if (file_exists("$diretorio_imagem_recortada.jpg")) {
            copy("$diretorio_imagem_recortada.jpg", "$destino_imagem_veiculo.jpg");
            unlink("$diretorio_imagem_recortada.jpg");
        }

        if (file_exists("$diretorio_imagem_recortada.png")) {
            copy("$diretorio_imagem_recortada.png", "$destino_imagem_veiculo.jpg");
            unlink("$diretorio_imagem_recortada.png");
        }
    }



}
