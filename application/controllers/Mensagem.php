<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 2/23/2019
 * Time: 10:32 AM
 */

class Mensagem extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model("Mensagem_model");
		}

		function index(){
			$this->read();
		}

		function carregarCadastro(){
			$this->load->view("sistema/mensagem/cadastro");
		}
		
		function create(){
			$data = array(
				'MENSAGEM' 		=> $this->input->post('MENSAGEM'),
				//'ID_USUARIO'	=> $this->input->post('ID_USUARIO'),
				//'ID_VAN'		=> $this->input->post('ID_VAN'),
				//'TITULO'		=> $this->input->post('TITULO')
			);
			$this->Mensagem_model->create($data);
			redirect('Mensagem');
		}

		function read(){
        	$this->data['mensagens'] = $this->Mensagem_model->read();
        	$this->load->view("sistema/mensagem/lista", $this->data);
        }

        function updateget(){
			$id = $this->uri->segment(3);
			$this->data['update'] = $this->Mensagem_model->getbyid($id);
			$this->load->view("sistema/mensagem/atualizacao", $this->data);
        }

        function updateset(){
			$data = array(
				'mensagem' 		=> $this->input->post('mensagem'),
				'id_usuario'	=> $this->input->post('id_usuario'),
				'id_van'		=> $this->input->post('id_van'),
				'titulo'		=> $this->input->post('titulo')
			);
			$this->Mensagem_model->updatesave($this->input->post('id'), $data);
			$this->data['update'] = $this->Mensagem_model->getbyid($this->input->post('id'));
			redirect('Mensagem');
		}

        function delete(){
			$id = $this->uri->segment(3);
			$this->Mensagem_model->delete($id);
        }
}
