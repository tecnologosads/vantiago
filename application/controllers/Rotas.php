<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 2/22/2019
 * Time: 7:46 PM
 */

class Rotas extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model("Rotas_model");
		}

		function index(){
			$this->read();
		}

		function carregarCadastro(){
		 
		    
		    $this->data['bairros'] = $this->Rotas_model->readCidadesEBairros();
		    $this->data['vans'] = $this->Rotas_model->exibirVanAluno();
		    
		    $this->load->view("sistema/rotas/cadastro", $this->data);
		}

		function create(){
			$data = array(
				'id_van' => $this->input->post('id_veiculo'),
				'link' => $this->input->post('link'),
				'nome_rota' => $this->input->post('nome_rota')
			);
			
			$this->Rotas_model->create($data);
			
			$dataVeiculo = array(
			    'id_veiculo' => $this->input->post('id_veiculo'),
			    'id_bairro' => $this->input->post('bairroVeiculo')
			);
			
			$this->Rotas_model->createVeiculo($dataVeiculo);
			
			
			
			redirect('Rotas');
		}

		function read(){
			$this->data['rotas'] = $this->Rotas_model->read();
        	$this->load->view("sistema/rotas/lista", $this->data);
		}

        function updateget(){
			$id = $this->uri->segment(3);
			$this->data['update'] = $this->Rotas_model->getbyid($id);
			$this->load->view("sistema/rotas/atualizacao", $this->data);
        }

        function updateset(){
			$data = array(
				'id_van' => $this->input->post('link'),
				'link' => $this->input->post('link'),
				'nome_rota' => $this->input->post('nome_rota')
			);
			$this->Rotas_model->updatesave($this->input->post('id'), $data);
			$this->data['update'] = $this->Rotas_model->getbyid($this->input->post('id'));
			redirect('Rotas');
		}

        function delete(){
			$id = $this->uri->segment(3);
			$this->Rotas_model->delete($id);
			redirect('Rotas');
        }
        
        function exibirBairros (){
            $exibirBairros = $this->rotas_model->readCidadesEBairros();
            $this->data['bairros'] = $exibirBairros;
        }
        
        
        
        
}
