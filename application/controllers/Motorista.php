<?php

class Motorista extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model("Motorista_model");
	}

	function index(){
		$this->read();
	}
	
	function carregarCadastro(){
		$this->load->view("sistema/motoristas/cadastro");
	}

	function create(){
		$data= array(
			"ID_TIPO_USUARIO" => 5,
			"NOME" => $this->input->post("NOME"),
			"CPF" => $this->input->post("CPF"),
			"RG" => $this->input->post("RG"),
			"TELEFONE" => $this->input->post("TELEFONE"),
			"TELEFONE2" => $this->input->post("TELEFONE2"),
			"ENDERECO" => $this->input->post("ENDERECO"),
			"EMAIL" => $this->input->post("EMAIL"),
			"SENHA" => md5($this->input->post("SENHA")),
			"CNH" => md5($this->input->post("CNH")),
			"VALIDADE_CNH" => md5($this->input->post("VALIDADE_CNH"))
		);
		$this->Motorista_model->create($data);
		redirect('Motorista');
	}

	function read(){
		$this->data['motoristas'] =  $this->Motorista_model->read();
		$this->load->view("sistema/motoristas/lista", $this->data);
	}

	function updateset(){
		$data= array(
			"NOME" => $this->input->post("NOME"),
			"CPF" => $this->input->post("CPF"),
			"RG" => $this->input->post("RG"),
			"TELEFONE" => $this->input->post("TELEFONE"),
			"TELEFONE2" => $this->input->post("TELEFONE2"),
			"ENDERECO" => $this->input->post("ENDERECO"),
			"EMAIL" => $this->input->post("EMAIL"),
			"SENHA" => md5($this->input->post("SENHA")),
			"CNH" => md5($this->input->post("CNH")),
			"VALIDADE_CNH" => md5($this->input->post("VALIDADE_CNH"))
		);
		$this->Motorista_model->updatesave($this->input->post('id'), $data);
		$this->data['update'] = $this->Motorista_model->getbyid($this->input->post('id'));
		redirect('Motorista');
	}

	function updateget(){
		$id = $this->uri->segment(3);
		$this->data['update'] = $this->Motorista_model->getbyid($id);
		$this->load->view("sistema/motoristas/atualizacao", $this->data);
	}

	function delete(){
		$id = $this->uri->segment(3);
		$this->Motorista_model->delete($id);
		redirect('Motorista');
	}

	function carregarPerfil(){
		//		Carregar model
		$this->load->model("Veiculo_model");

		//		Carregar o array
		$this->data['quantidades'] = $this->Veiculo_model->carregarQuatidadeVeiculos($this->uri->segment(3));
		$this->data['perfils'] = $this->Motorista_model->getbyid($this->uri->segment(3));


		//		Carregar a view
		$this->load->view("sistema/motoristas/perfil", $this->data);
	}
}
