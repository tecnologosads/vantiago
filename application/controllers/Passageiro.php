<?php
class Passageiro extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model("Passageiro_model");
		}

		function index(){
			$this->read();
		}
		
		function carregarCadastro(){
			$this->data['estados'] = $this->Passageiro_model->carregarEstados();
 			$this->load->view("sistema/passageiro/cadastro", $this->data);
		}

		function create(){
			$data = array(
				'ID_TIPO_USUARIO' 		=> 6,
				'NOME' 		=> $this->input->post('NOME'),
				'IDADE' 	=> $this->input->post('IDADE'),
				'TURNO' 	=> $this->input->post('TURNO'),
				'TELEFONE' 	=> $this->input->post('TELEFONE'),
				'ENDERECO' 	=> $this->input->post('ENDERECO')
			);
			$this->Passageiro_model->create($data);
			redirect('Passageiro');
		}

		function read(){
        	$this->data['passageiros'] = $this->Passageiro_model->read();
        	$this->load->view("sistema/passageiro/lista", $this->data);
		}

        function updateget(){
			$id = $this->uri->segment(3);
			$this->data['update'] = $this->Passageiro_model->getbyid($id);
			$this->data['estados'] = $this->Passageiro_model->carregarEstados();
			$this->load->view("sistema/passageiro/atualizacao", $this->data);
        }

        function updateset(){
			$data = array(
				'NOME' 		=> $this->input->post('NOME'),
				'IDADE' 	=> $this->input->post('IDADE'),
				'TURNO' 	=> $this->input->post('TURNO'),
				'TELEFONE' 	=> $this->input->post('TELEFONE'),
				'ENDERECO' 	=> $this->input->post('ENDERECO')
			);
			$this->Passageiro_model->updatesave($this->input->post('id'), $data);
			$this->data['update'] = $this->Passageiro_model->getbyid($this->input->post('id'));
			$this->data['perfils'] = $this->Passageiro_model->getbyid($this->input->post('id'));
			//$this->load->view("sistema/passageiro/perfil", $this->data);
			redirect('/');
		}

        function delete(){
			$id = $this->uri->segment(3);
			$this->Passageiro_model->delete($id);
        	redirect('Passageiro');
		}

		function carregarPerfil(){
//			Carregar model
			$this->load->model("Cidade_model");

//			Passando para o array
		$this->data['perfils'] = $this->Passageiro_model->getbyid($this->uri->segment(3));


//			Passando informações para a view
		$this->load->view("sistema/passageiro/perfil", $this->data);
	}

}
