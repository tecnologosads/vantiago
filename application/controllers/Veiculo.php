<?php
defined('BASEPATH') OR exist('No direct script acess allowed');

class Veiculo extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Veiculo_model");
//
//        $logado = $this->session->userdata('nome');
//        if(empty($logado)){
//            redirect('/');
//        }
    }

    function index()
    {
        $this->read();
    }

    function loadcreate()
    {
        $exibirCidades = $this->Veiculo_model->readCidades();
        $this->data['cidades'] = $exibirCidades;
       
        $exibirBairros = $this->Veiculo_model->readBairros();
        $this->data['bairros'] = $exibirBairros;
        
        $this->load->view("sistema/veiculos/create", $this->data);
    }

    function create()
    {
        $data = array(
            "id_pessoa" => $this->session->userdata('id'),
            "ano" => $this->input->post("ano"),
            "modelo" => $this->input->post("modelo"),
            "id_tipo_veiculo" => $this->input->post("tipo_veiculo"),
            "fabricante" => ($this->input->post("fabricante")),
            "renavan" => ($this->input->post("renavan")),
            "chassi" => ($this->input->post("chassi")),
            "qtd_lugares" => ($this->input->post("quantidadedelugares")),
            "placa" => $placa = ($this->input->post("placa")),
            "descricao_veiculo" => ($this->input->post("descricao_veiculo"))
        );
        $id_salvo = $this->Veiculo_model->create($data);

        $this->salvarImagem($placa);

        $id_bairro = ($this->input->post("valueVeiculoBairro"));

        $this->createCidades($id_salvo, $id_bairro);
        echo $this->createTurnoVan($id_salvo);

        redirect('veiculo');
    }

    function createCidades($id_salvo, $id_bairro)
    {
        $i = 0;
        foreach ($id_bairro as $b) {

            $dataVeiculo = array(
                'ID_VEICULO' => $id_salvo,
                'ID_BAIRRO' => $id_bairro[$i],
            );

            $i++;
            $this->Veiculo_model->createVeiculo($dataVeiculo);
        }

    }

    function createTurnoVan($id_veiculo)
    {
        $id_turno_matutino = $this->input->post("customRadio1");
        $id_turno_noturno = $this->input->post("customRadio2");


        if ($id_turno_matutino == 1) {
            $data = array(
                'VEICULOS_ID' => $id_veiculo,
                'TURNOS_ID' => $id_turno_matutino,
            );

            $this->Veiculo_model->createTurnoVan($data);
        }

        if ($id_turno_noturno == 2) {
            $data = array(
                'VEICULOS_ID' => $id_veiculo,
                'TURNOS_ID' => $id_turno_noturno,
            );

            $this->Veiculo_model->createTurnoVan($data);
        }
    }

    function gerarJsonCidades()
    {
        $id_categoria = $this->uri->segment(3);
        $exibirCidades = $this->Veiculo_model->readCidadesEBairrosJson($id_categoria);
        return print json_encode($exibirCidades);
    }

    function read()
    {
        $lista = $this->Veiculo_model->read();
        $this->data['veiculos'] = $lista;
        $this->load->view("sistema/veiculos/veiculo", $this->data);
    }

    function update()
    {
        $id = $this->uri->segment(3);
        $this->data['updates'] = $this->Veiculo_model->getbyid($id);
        $this->load->view("sistema/veiculos/update", $this->data);
    }

    function listarPessoa()
    {
        $id = $this->uri->segment(3);
        $this->data['updates'] = $this->Veiculo_model->getbyid($id);
        $this->load->view("sistema/veiculos/update", $this->data);
    }

    function updatesave()
    {
        $data = array(
            "ano" => $this->input->post("ano"),
            "modelo" => $this->input->post("modelo"),
            "id_tipo_veiculo" => $this->input->post("tipo_veiculo"),
            "fabricante" => ($this->input->post("fabricante")),
            "renavan" => ($this->input->post("renavan")),
            "chassi" => ($this->input->post("chassi")),
            "qtd_lugares" => ($this->input->post("quantidadedelugares")),
            "placa" => $placa = ($this->input->post("placa")),
            "descricao_veiculo" => ($this->input->post("descricao_veiculo"))
        );

        $this->Veiculo_model->updatesave($this->input->post('idsave'), $data);
        $this->data['updates'] = $this->Veiculo_model->getbyid($this->input->post('idsave'));

        $this->load->view('sistema/veiculos/update', $this->data);
        $this->salvarImagem($placa);
        redirect('veiculo');
    }

    function delete()
    {
        $id = $this->uri->segment(3);
        $this->Veiculo_model->deleteRota($id);
        $this->Veiculo_model->deleteTurnoVan($id);
        $this->Veiculo_model->delete($id);
        redirect('veiculo');
    }

    function carregarPerfil()
    {
        $this->data['perfils'] = $this->Veiculo_model->getbyid($this->uri->segment(3));
        $this->load->view("sistema/veiculos/perfil", $this->data);
    }


    function salvarImagem($placa)
    {
        $nome_imagem = $this->session->userdata('link_foto');

        $diretorio_imagem_recortada = "assets/images/img_crop/$nome_imagem";

        $destino_imagem_veiculo = "assets/sistema/images/imagens_veiculos/$placa";

        if (file_exists("$diretorio_imagem_recortada.jpg")) {
            copy("$diretorio_imagem_recortada.jpg", "$destino_imagem_veiculo.jpg");
            unlink("$diretorio_imagem_recortada.jpg");
        }

        if (file_exists("$diretorio_imagem_recortada.png")) {
            copy("$diretorio_imagem_recortada.png", "$destino_imagem_veiculo.jpg");
            unlink("$diretorio_imagem_recortada.png");
        }
    }

    function editarImagemUpload()
    {
        $this->load->library('image_lib');
    }


    function vanDestaque()
    {
        $lista = $this->Veiculo_model->readProprietarioVeiculo();
        $this->data['vanDestaque'] = $lista;
        $this->load->view("sistema/home/home", $this->data);
    }


    function exibirBairros()
    {
        $exibirBairros = $this->Veiculo_model->readCidadesEBairros();
        $this->data['bairros'] = $exibirBairros;
    }
}
