<div class="form-group m-b-25">
	<div class="col-12">
		<button type="button" class="btn btn-primary waves-light waves-effect" data-toggle="modal" data-target="#signup-modal">Nova mensagem</button>
	</div>
</div>
<?php foreach ($mensagens as $mensagem) { ?>
	<br>
	<ul class="sortable-list taskList list-unstyled" id="upcoming">
		<li class="task-warning" id="task1">
			<?php echo $mensagem->mensagem?>
			<div class="clearfix"></div>
			<div class="mt-3">
				<p class="pull-right mb-0 mt-2">
					<a href="<?php echo base_url(); ?>index.php/Mensagem/updateget/<?php echo $mensagem->id ?>" class="btn btn-success waves-effect waves-light">
						Editar
					</a>
				</p>
				<p class="mb-0"><a href="" class="text-muted"> <span class="font-bold font-secondary"><?php echo $mensagem->id?> Data e hora postada</span></a> </p>
			</div>
		</li>
	</ul>
<?php } ?>
<div class="col-lg-8">
	<div class="card-box ribbon-box">
		<div class="ribbon ribbon-dark">Anderson Andrade</div>
		<p class="m-b-0">
			Hoje não teremos aula na faculdade.
		</p>
	</div>
</div>
<div class="col-lg-8">
	<div class="card-box ribbon-box">
		<div class="ribbon ribbon-success">Romulo Cavalcante</div>
		<p class="m-b-0">
			Por ter um evento no dia 12 de julho as aulas vão terminar mais cedo.
		</p>
	</div>
</div>
<div class="col-lg-8">
	<div class="card-box ribbon-box">
		<div class="ribbon ribbon-two-danger">Tiago Silva</div>
		<p class="m-b-0">Toda a minha turma foi libertada mais cedo.</p>
	</div>
</div>
<div class="col-lg-8">
	<div class="card-box ribbon-box">
		<div class="ribbon ribbon-box">Matheus Felipe</div>
		<p class="m-b-0">Não vou para a faculdade hoje.</p>
	</div>
</div>
