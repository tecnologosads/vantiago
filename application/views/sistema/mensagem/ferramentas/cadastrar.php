<div id="signup-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
				<form class="form-horizontal" method="post" action="<?php echo base_url();?>index.php/Mensagem/create">

					<div class="form-group m-b-25">
						<div class="col-12">
							<label for="username">Nome do usuario</label>
						</div>
					</div>

					<div class="form-group">
						<label>Mensagem</label>
						<input name="MENSAGEM" placeholder="Digite sua mensagem aqui: " id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
					</div>

					<div class="form-group account-btn text-center m-t-10">
						<div class="col-12">
							<button class="btn w-lg btn-rounded btn-primary waves-effect waves-light" type="submit">Salvar mensagem</button>
						</div>
					</div>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
