<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 3/26/2019
 * Time: 11:09 PM
 */
?>
<!-- Start Page content -->
<div class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<!-- meta -->
				<div class="profile-user-box card-box bg-custom">
					<div class="row">
						<div class="col-sm-6">
<!--							<span class="pull-left mr-3"><img src="assets/images/users/avatar-1.jpg" alt="" class="thumb-lg rounded-circle"></span>-->
							<div class="media-body text-white">
								<h4 class="mt-1 mb-1 font-18"><?php echo $perfils->MODELO?></h4>
								<p class="font-13 text-light"> <?php echo $perfils->FABRICANTE?></p>
								<p class="text-light mb-0"><?php echo $perfils->ANO?></p>
							</div>
						</div>
<!--						Editar perfil-->
<!--						<div class="col-sm-6">-->
<!--							<div class="text-right">-->
<!--								<button type="button" class="btn btn-light waves-effect">-->
<!--									<i class="mdi mdi-account-settings-variant mr-1"></i> Editar perfil-->
<!--								</button>-->
<!--							</div>-->
<!--						</div>-->
					</div>
				</div>
				<!--/ meta -->
			</div>
		</div>
		<!-- end row -->


		<div class="row">
			<div class="col-md-4">
				<!-- Personal-Information -->
				<div class="card-box">
					<div class="panel-body">
						<h4 class="header-title mt-0 m-b-20">Descrição do veiculo</h4>

						<p class="text-muted font-13">
							<?php echo $perfils->DESCRICAO_VEICULO?>
						</p>

						<hr/>

						<div class="text-left">
							<h4 class="header-title mt-0 m-b-20">Informações</h4>

							<p class="text-muted font-13"><strong>Modelo :</strong> <span class="m-l-15"><?php echo $perfils->MODELO?></span></p>

							<p class="text-muted font-13"><strong>Fabricante :</strong><span class="m-l-15"><?php echo $perfils->FABRICANTE?></span></p>

							<p class="text-muted font-13"><strong>Quantidade de lugares:</strong><span class="m-l-15"><?php echo $perfils->QTD_LUGARES?></span></p>


						</div>

<!--						Redes sociais-->
<!--						<ul class="social-links list-inline m-t-20 m-b-0">-->
<!--							<li class="list-inline-item">-->
<!--								<a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>-->
<!--							</li>-->
<!--							<li class="list-inline-item">-->
<!--								<a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>-->
<!--							</li>-->
<!--							<li class="list-inline-item">-->
<!--								<a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>-->
<!--							</li>-->
<!--						</ul>-->
					</div>
				</div>
				<!-- Personal-Information -->

<!-- 				Mensagem -->
<!--				<div class="card-box ribbon-box">-->
<!--					<div class="ribbon ribbon-primary">Messages</div>-->
<!--					<div class="clearfix"></div>-->
<!--					<div class="inbox-widget">-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-2.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Tomaslau</p>-->
<!--								<p class="inbox-item-text">I've finished it! See you so...</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-3.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Stillnotdavid</p>-->
<!--								<p class="inbox-item-text">This theme is awesome!</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-4.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Kurafire</p>-->
<!--								<p class="inbox-item-text">Nice to meet you</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!---->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-5.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Shahedk</p>-->
<!--								<p class="inbox-item-text">Hey! there I'm available...</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-6.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Adhamdannaway</p>-->
<!--								<p class="inbox-item-text">This theme is awesome!</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!---->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-2.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Tomaslau</p>-->
<!--								<p class="inbox-item-text">I've finished it! See you so...</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-3.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Stillnotdavid</p>-->
<!--								<p class="inbox-item-text">This theme is awesome!</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--					</div>-->
<!--				</div>-->

			</div>


			<div class="col-md-8">

				<div class="row">

					<div class="col-sm-4">
						<div class="card-box tilebox-one">
							<h6 class="text-muted text-uppercase mt-0">Quatidade de vagas restantes</h6>
							<h2 class="m-b-20" data-plugin="counterup">Ao todo <?php echo $perfils->QTD_LUGARES?></h2>
							<span class="badge badge-custom"> +11% </span> <span class="text-muted">Restantes</span>
						</div>
					</div><!-- end col -->

					<div class="col-sm-4">
						<div class="card-box tilebox-one">
							<h6 class="text-muted text-uppercase mt-0">Quantidade de mensagens</h6>
							<h2 class="m-b-20"><span data-plugin="counterup">#######</span></h2>
						</div>
					</div><!-- end col -->

<!--					Mais uma opção caso seja necessária-->
<!--					<div class="col-sm-4">-->
<!--						<div class="card-box tilebox-one">-->
<!--							<h6 class="text-muted text-uppercase mt-0">Product Sold</h6>-->
<!--							<h2 class="m-b-20" data-plugin="counterup">1,890</h2>-->
<!--							<span class="badge badge-custom"> +89% </span> <span class="text-muted">Last year</span>-->
<!--						</div>-->
<!--					</div>
end col -->

				</div>
				<!-- end row -->


				<div class="card-box">
					<h4 class="header-title mt-0 mb-3">Mapa</h4>
					<div class="">
						<div class="">
							<h5 class="text-custom m-b-5">Nome da rota</h5>
						</div>
							O mapa vem aqui
						</div>

					</div>
				</div>

				<div class="card-box">
					<h4 class="header-title mb-3">Passageiros</h4>

					<div class="table-responsive">
						<table class="table m-b-0">
							<thead>
							<tr>
								<th>Nome</th>
								<th>Entrada</th>
								<th>Turno</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>Adminox Admin</td>
								<td>01/01/2015</td>
								<td>Noturno</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			<!-- end col -->

		</div>
		<!-- end row -->


	</div> <!-- container -->

</div> <!-- content -->

