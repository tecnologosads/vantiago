
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h1 class="m-t-0">Cadastro de Veículos</h1>
            <form method="post" novalidate="" class="row" enctype="multipart/form-data"
                  action="<?php echo base_url(); ?>index.php/Veiculo/create">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <?php $this->load->view("sistema/htmlelements/botaoAdicionarFoto2.html"); ?>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Modelo<span class="text-danger">*</span></label>
                        <input name="modelo" placeholder="modelo" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Fabricante<span class="text-danger">*</span></label>
                        <input name="fabricante" required="" placeholder="fabricante" class="form-control">
                    </div>

                    <div class="form-group col-md-1">
                        <label>Ano<span class="text-danger">*</span></label>

                        <select name="ano" required="" class="form-control">
                            <?php
                            $dataAtual = date('Y');
                            for ($i = $dataAtual - 10; $i <= $dataAtual; $i++) {
                                echo "<option>$i</option>";
                            }
                            ?>
                        </select>

                    </div>

                    <div class="form-group col-md-2">
                        <label>Tipo de Veículo<span class="text-danger">*</span></label>
                        <select name="tipo_veiculo" required="" class="form-control">
                            <option value="1" selected>VAN</option>
                            <option value="3">MICRO ÔNIBUS</option>
                            <option value="2">ÔNIBUS</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Renavan<span class="text-danger">*</span></label>
                        <input name="renavan" minlength="" required="" placeholder="renavan"
                               class="form-control inteiro11">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Chassi<span class="text-danger">*</span></label>
                        <input maxlength="17" name="chassi" required="" placeholder="chassi" class="form-control">
                    </div>

                    <div class="form-group col-md-2">
                        <label>QTD lugares<span class="text-danger">*</span></label>
                        <select name="quantidadedelugares" required="" class="form-control">
                            <?php
                            for ($i = 5; $i <= 100; $i++) {
                                echo "<option>$i</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-md-2">
                        <label>Placa<span class="text-danger">*</span></label>
                        <input name="placa" required="" placeholder="placa" class="form-control placa">
                    </div>

                    <div class="form-group col-md-7">
                        <label>Descrição do veículo<span class="text-danger">*</span></label>
                        <textarea class="form-control" name="descricao_veiculo" rows="5"
                                  style="height: 95px;"></textarea>
                    </div>

                    <div class="form-group col-md-5">
                        <label>Turnos de atendimento da Van<span class="text-danger">*</span></label>

                        <div class="col-md-12 border div_turnos rounded">
                            <!--                            <p class="text-muted font-14 m-b-25">Selecione os turnos em que a van atende aos alunos.</p>-->
                            <div class="div_turnos_dentro col-md-6 border-right">
                                <h5>Matutino</h5>
                                <div class="custom-control custom-radio col-md-5">
                                    <input type="radio" id="customRadio1" value="1" name="customRadio1"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Sim</label>
                                </div>
                                <div class="custom-control custom-radio col-md-5">
                                    <input type="radio" id="customRadio2" value="99" name="customRadio1"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Não</label>
                                </div>
                            </div>

                            <div class="div_turnos_dentro col-md-6">
                                <h5>Noturno</h5>
                                <div class="custom-control custom-radio col-md-5">
                                    <input type="radio" id="customRadio3" value="2" name="customRadio2"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Sim</label>
                                </div>
                                <div class="custom-control custom-radio col-md-5">
                                    <input type="radio" id="customRadio4" value="99" name="customRadio2"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Não</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h4 class="m-t-0 header-title">Regiões Atendidas</h4>
                    <p class="text-muted font-14">
                        Selecione as cidades atendidas pela van, em seguida marque os bairros atendidos.
                    </p>

                    <table class="table table-striped">
                        <select id="id_categoria" class="form-control">
                            <?php
                            foreach ($cidades as $c):
                                echo "<option value='$c->ID'> $c->CIDADE </option>";
                            endforeach;
                            ?>
                        </select>

                        <thead>
                        <tr>
                            <th>Bairro</th>
                        <tr>
                        <thead>

                        <tbody>

                        <td id="id_sub_categoria"></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                    

                	
                    <ul id="caixa_bairro" class="col-md-6">

                    </ul>

                </div>
                
                
                <div class="form-group row float-right">
                    <button type="submit" class=" btn btn-info waves-effect waves-light"><i class="fa mdi mdi-content-save m-r-5"></i> <span>Cadastrar</span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar e recortar imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe class="iframe_carregar_imagem" src="<?= base_url() ?>index.php/base"></iframe>
            </div>

        </div>
    </div>
</div>


<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>

<?php $this->load->helper("jssistemadown_helper") ?>


<script type="text/javascript">
    $(document).ready(function () {  console.log(1111);
        $('#id_categoria').change(function () {
            var valorCidade = $(this).val();
            var url = "<?php echo base_url() ?>index.php/veiculo/gerarJsonCidades/" + valorCidade;

            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var options = '';

                    var i = 1;
                    for (var i = 0; i < data.length; i++) {
                        var checked = '';
                        $('#caixa_bairro li').each(function () {
                            if($(this).attr('data-id')== data[i].id)  {
                                checked = 'checked';
                            }
                        });

                        options += '<div class="custom-control custom-checkbox bairros">';
                        options += '<input '+checked+' type="checkbox" data-id="'+ [i] +'" id="customCheck'+ [i] +'" class="custom-control-input listar-bairros" name="id_bairro[]" value="' + data[i].id + '">' + '</input>';
                        options += '<label id="labelBairro' + [i] +'" class="custom-control-label labelCidades" for="customCheck' + [i] +'">'+ data[i].nome_sub_categoria + '</label>';
                        options += '</div>';
                        i++
                    }
                    $('#id_sub_categoria').html(options).show();

                },
            });
        });

        $('#id_sub_categoria').on('click', '.listar-bairros', function () {

            var elementoBairro = $(this);
            var idBairro = elementoBairro.val();

            if(elementoBairro.is(':checked')){
                var iBairro = elementoBairro.attr('data-id');

                console.log(iBairro);
                var liAdicionarBairro = '<li id="li'+idBairro+'" data-id="'+ idBairro +'"><input type="hidden" name="valueVeiculoBairro[]" value="'+ idBairro +'">' + $('#id_categoria option:selected').text() + ' - ' + $('#labelBairro' + iBairro).html() + '</li>';

                console.log(liAdicionarBairro);

                $('#caixa_bairro').append(liAdicionarBairro);
            }else{
                $('#li'+idBairro).remove();
            }

        });



    });
</script>

