
<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
            <h1 class="m-t-0">Editar Veículo</h1>
			<form method="post" novalidate="" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/veiculo/updatesave">
                <input type="hidden" name="idsave" value="<?php echo $updates->ID?>">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <?php $this->load->view("sistema/htmlelements/botaoAdicionarFoto2.html"); ?>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Modelo<span class="text-danger">*</span></label>
                        <input name="modelo" value="<?php echo $updates->MODELO?>" placeholder="modelo" required=""  class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Fabricante<span class="text-danger">*</span></label>
                        <input name="fabricante" value="<?php echo $updates->FABRICANTE?>" required="" placeholder="fabricante"  class="form-control">
                    </div>

                    <div class="form-group col-md-1">
                        <label>Ano<span class="text-danger">*</span></label>
                        
                        <select name="ano" required="" class="form-control">
                            <option selected><?php echo $updates->ANO?></option>
                            <?php
                                $dataAtual = date('Y');
                                for($i= $dataAtual - 10; $i<= $dataAtual; $i++){
                                    echo "<option>$i</option>";
                                }
                            ?>
                        </select>
                        
                    </div>
                    
                    <div class="form-group col-md-2">
                        <label>Tipo de Veículo<span class="text-danger">*</span></label>
                        <select name="tipo_veiculo" required="" class="form-control">
                            <option value="1">VAN</option>
                            <option value="3">MICRO ÔNIBUS</option>
                            <option value="2">ÔNIBUS</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Renavan<span class="text-danger">*</span></label>
                        <input name="renavan" value="<?php echo $updates->RENAVAN?>" minlength="" required="" placeholder="renavan"  class="form-control inteiro11">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Chassi<span class="text-danger">*</span></label>
                        <input maxlength="17" value="<?php echo $updates->CHASSI?>" name="chassi" required="" placeholder="chassi"  class="form-control">
                    </div>

                    <div class="form-group col-md-2">
                        <label>QTD. lugares<span class="text-danger">*</span></label>
                        <select name="quantidadedelugares" required="" class="form-control">
                        <option selected><?php echo $updates->QTD_LUGARES?></option>
                            <?php
                                for($i=5; $i<=100; $i++){
                                    echo "<option>$i</option>";
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-md-2">
                        <label>Vagas Disponíveis<span class="text-danger">*</span></label>
                    </div>

                    
                    <div class="form-group col-md-3">
                        <label>Placa<span class="text-danger">*</span></label>
                        <input name="placa" value="<?php echo $updates->PLACA?>" required="" placeholder="placa"  class="form-control placa">
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label>Descrição do veículo<span class="text-danger">*</span></label>
                        <textarea class="form-control"  name="descricao_veiculo" rows="5" style="height: 150px;"><?php echo $updates->DESCRICAO_VEICULO?></textarea>
                    </div>
                </div>

                    <div class="form-group row float-right">
                        <button type="submit" class=" btn btn-info waves-effect waves-light"> <i class="fa mdi mdi-content-save m-r-5"></i> <span>Atualizar</span> </button>
                    </div>
                </form>
		</div>
	</div>
</div>
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar e recortar imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe class="iframe_carregar_imagem" src="<?= base_url() ?>index.php/base"></iframe>
            </div>

        </div>
    </div>
</div>

