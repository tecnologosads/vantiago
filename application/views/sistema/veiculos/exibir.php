<div class="row">
	<div class="card-box table-responsive">
		<div class="col-12">
			<table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0"
				   width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
				<h4 class="m-t-0 header-title">Lista de Veiculos</h4>
				<thead>
				<tr>
					<th>Imagem</th>
					<th>#</th>
					<th>Ano</th>
					<th>Modelo</th>
					<th>Fabricante</th>
					<th>Renavan</th>
					<th>QTD. Lugares</th>
					<th>Placa</th>
					<th><i class="dripicons-trash"></i></th>
					<th><i class="dripicons-pencil"></i></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($veiculos as $veiculo): ?>
					<tr role="row" class="odd">
						<td class="sorting_1"><img class="imagem_aluno" src="<?=  $diretorio = base_url()."assets/sistema/images/imagens_veiculos/$veiculo->PLACA" . ".jpg";?> "></td>
                        <th scope="row"><?php echo $veiculo->ID ?></th>
						<td class="sorting_1"><?php echo $veiculo->ANO ?></td>
						<td class="sorting_1"><?php echo $veiculo->MODELO ?></td>
						<td class="sorting_1"><?php echo $veiculo->FABRICANTE ?></td>
						<td class="sorting_1"><?php echo $veiculo->RENAVAN ?></td>
						<td class="sorting_1"><?php echo $veiculo->QTD_LUGARES ?></td>
						<td class="sorting_1"><?php echo $veiculo->PLACA ?></td>
						<td>
							<a href="<?php echo base_url(); ?>index.php/veiculo/delete/<?php echo $veiculo->ID ?>"><i
									class="dripicons-trash"></i></a>
                        </td>
                        
						<td>
							<a href="<?php echo base_url(); ?>index.php/veiculo/update/<?php echo $veiculo->ID ?>"><i
									class="dripicons-pencil"></i></a>
                        </td>
                        
                        
                        
					</tr>
				<?php endforeach; ?>
				</tbody>
		</div>
	</div>
</div>

