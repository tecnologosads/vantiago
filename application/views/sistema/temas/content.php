<div class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-12">
				<?php $this->load->view("sistema/temas/cardbox") ?>
			</div>
		</div>
		<!-- end row -->



		<div class="row">
			<div class="col-lg-6">
				<?php $this->load->view("sistema/temas/overview") ?>
			</div>

			<div class="col-lg-6">
				<?php $this->load->view("sistema/temas/salesoverview") ?>
			</div>
		</div>
		<!-- end row -->


		<div class="row">
			<div class="col-lg-8">
				<?php $this->load->view("sistema/temas/walletbalances") ?>
			</div>

			<div class="col-lg-4">
				<?php $this->load->view("sistema/temas/totalwallet") ?>
			</div>
		</div>
		<!-- end row -->




	</div> <!-- container -->

</div> <!-- content -->
