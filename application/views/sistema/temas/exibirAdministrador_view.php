<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Login Sistema</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <?php $this->load->helper("csssistema_helper") ?>
    <?php $this->load->helper("jssistematop_helper") ?>
</head>
<body>

<div id="wrapper">
    <?php $this->load->view("sistema/temas/menu/verticalmenu") ?>

    <div class="content-page">
        <div class="topbar">
            <?php $this->load->view("sistema/temas/navbar") ?>
        </div>

        <div class="card-box">
            <?php $this->load->view("sistema/temas/tabelas/tabelaAdministrador") ?>
        </div>
    </div>
</div>
<?php $this->load->helper("jssistemadown_helper") ?>
</body>
</html>
