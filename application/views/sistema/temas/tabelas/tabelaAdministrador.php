<div class="row">
    <div class="card-box table-responsive">
        <div class="col-12">

			<table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
                <thead>
                <tr role="row">
                    <th>#</th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>Telefone</th>
                    <th>Telefone²</th>
                    <th>Endereco</th>
                    <th>Email</th>
                    <th>Login</th>
                    <th>Editar</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($administradores as $admin):?>
                    <tr role="row" class="odd">
                        <th scope="row"><?= $admin ['ID']?></th>
                        <td class="sorting_1"><?= $admin ['NOME']?></td>
                        <td><?= $admin ['CPF']?></td>
                        <td><?= $admin ['RG']?></td>
                        <td><?= $admin ['TELEFONE']?></td>
                        <td><?= $admin ['TELEFONE2']?></td>
                        <td><?= $admin ['ENDERECO']?></td>
                        <td><?= $admin ['EMAIL']?></td>
                        <td><?= $admin ['LOGIN']?></td>
                        <td>

<!--                            <form action="--><?php //echo base_url() . 'index.php/admin/excluirAdministrador/' .  $admin['ID'] ?><!--" method="get">-->
<!--                                <button class="btn btn-light waves-effect waves-light btn-sm" id="sa-warning">-->
<!--                                    <i class="mdi mdi-delete icone_deletar"></i>-->
<!--                                </button>-->
<!--                            </form>-->
                            <a href="<?php echo base_url() . 'index.php/admin/excluirAdministrador/' .  $admin['ID'] ?>">
                                <i class="mdi mdi-delete icone_deletar"></i>
                            </a>


                            <a href="<?php echo base_url() . 'index.php/admin/exibirAdministradorEditar/' .  $admin['ID'] ?>">
                                <i class="mdi mdi-account-edit icone_editar"></i>
                            </a>
                        </td>
                    </tr>

                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
