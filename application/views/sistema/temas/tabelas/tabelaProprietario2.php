<div class="row">
    <div class="col-lg-10">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Lista de Administradores</h4>
<!--            <p class="text-muted font-14 m-b-20">-->
<!--                For basic styling—light padding and only horizontal dividers—add the base class <code>.table</code> to any <code>&lt;table&gt;</code>.-->
<!--            </p>-->

            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>CNPF</th>
                    <th>RG</th>
                    <th>NOME</th>
                    <th>TELEFONE</th>
                    <th>ENDEREÇO</th>
                    <th>EMAIL</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($proprietarios as $proprietario): ?>
                <tr>
                    <th scope="row"><?= $proprietario ['id']?></th>
                    <td><?= $proprietario ['cnpj']?></td>
                    <td><?= $proprietario ['rg']?></td>
                    <td><?= $proprietario ['nome']?></td>
                    <td><?= $proprietario ['telefone']?></td>
                    <td><?= $proprietario ['endereco']?></td>
                    <td><?= $proprietario ['email']?></td>
                </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>

    </div>
</div>