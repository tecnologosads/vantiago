<div class="row">
    <div class="col-lg-10">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Lista de Administradores</h4>

            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Placa</th>
                    <th>Modelo</th>
                    <th>Registro</th>
                    <th>Linha</th>
                    <th>Ano</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($veiculos as $veiculo): ?>
                <tr>
                    <th scope="row"><?= $veiculo ['id']?></th>
                    <td><?= $veiculo ['placa']?></td>
                    <td><?= $veiculo ['modelo']?></td>
                    <td><?= $veiculo ['registro']?></td>
                    <td><?= $veiculo ['linha']?></td>
                    <td><?= $veiculo ['ano']?></td>
                </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>

    </div>
</div>