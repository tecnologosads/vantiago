<div class="row">
	<div class="col-md-6">
		<div class="card-box">
			<h4 class="m-t-0 m-b-30 header-title">Cadastrar Van</h4>



            <?php
            echo form_open("van/cadastrarVan");

            ?> <div class="form-group"><?php
                echo form_label("Placa");
                echo form_input(array(
                   "name" => "placa",
                    "id" => "exampleInputEmail1",
                    "class" => "form-control",
                    "placeholder"=>"Placa",
                    "aria-describedby"=>"emailHelp",
            ));

            ?></div> <div class="form-group"><?php
                echo form_label("Modelo");
                echo form_input(array(
                    "name" => "modelo",
                    "id" => "exampleInputEmail1",
                    "class" => "form-control",
                    "placeholder"=>"Modelo",
                    "aria-describedby"=>"emailHelp",
                ));

            ?></div> <div class="form-group"><?php
                echo form_label("Registro");
                echo form_input(array(
                    "name" => "registro",
                    "id" => "exampleInputEmail1",
                    "class" => "form-control",
                    "placeholder"=>"Registro",
                    "aria-describedby"=>"emailHelp",
                ));

            ?></div> <div class="form-group"><?php
            echo form_label("Linha");
            echo form_input(array(
                "name" => "linha",
                "id" => "exampleInputEmail1",
                "class" => "form-control",
                "placeholder"=>"Linha",
                "aria-describedby"=>"emailHelp",
            ));

            ?></div> <div class="form-group"><?php
                echo form_label("Ano");
                echo form_input(array(
                    "name" => "ano",
                    "id" => "exampleInputEmail1",
                    "class" => "form-control",
                    "placeholder"=>"Ano",
                    "aria-describedby"=>"emailHelp",
                ));
                ?></div><?php


            echo form_button(array(
                "type" => "submit",
                "class" => "btn btn-primary",
                "content" => "Cadastrar"
            ));
            echo form_close();


            ?>

		</div>
	</div>
</div>
