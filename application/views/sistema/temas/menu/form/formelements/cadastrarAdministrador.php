<div class="row">
	<div class="col-md-6">
		<div class="card-box">
			<h4 class="m-t-0 m-b-30 header-title">Cadastrar Administrador</h4>

            <form method="post" action="<?php echo base_url();?>index.php/admin/novoadministrador">
                <div class="form-group">
                    <label>Nome</label>
                    <input style="display: none" name="id_tipo_usuario" value="1">

                    <input name="nome" placeholder="Nome" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>CPF</label>
                    <input name="cpf" placeholder="Nome" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>RG</label>
                    <input name="rg" placeholder="RG" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Telefone¹</label>
                    <input name="telefone" placeholder="Telefone1" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Telefone²</label>
                    <input name="telefone2" placeholder="Telefone²" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Endereço</label>
                    <input name="endereco" placeholder="Endereço" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input name="email" placeholder="Email" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Login</label>
                    <input name="login" placeholder="Login" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Senha</label>
                    <input name="senha" id="exampleInputPassword" class="form-control"  placeholder="Senha" type="password">
                </div>

                <button type="submit" class="btn btn-primary"> Cadastrar</button>
            </form>
		</div>
	</div>
</div>
