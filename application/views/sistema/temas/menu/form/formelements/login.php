<div class="row">
	<div class="col-md-5">
		<div class="card-box">
			<h4 class="m-t-0 m-b-30 header-title">Entrar</h4>

            <?php echo form_open("login/autenticar");
                echo '<div class="form-group">';
                    echo form_label("Login");
                    echo form_input(array(
                            "class" => "form-control",
                            "name" => "login",
                            "id" => "exampleInputEmail1",
                            "aria-describedby"=> "emailHelp",
                            "placeholder"=>"Entre com seu Email"
                    ));
                     echo '<small id="emailHelp" class="form-text text-muted">Favor digite o seu login para acessar.</small>';
                echo '</div>';

                echo '<div class="form-group">';
                    echo form_label("Senha");
                    echo form_password(array(
                        "class" => "form-control",
                        "name" => "senha",
                        "id" => "exampleInputPassword1",
                        "placeholder"=>"Senha"
                    ));
                echo '</div>';
                echo form_button(array(
                    "type" => "submit",
                    "class" => "btn btn-primary",
                    "content" => "Entrar"
                ));
            echo form_close(); ?>

		</div>
	</div>
</div>
