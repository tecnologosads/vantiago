<div class="row">
	<div class="col-md-6">
		<div class="card-box">
			<h4 class="m-t-0 m-b-30 header-title">Cadastrar Administrador</h4>

            <form method="post" action="novoadministrador">
                <?php foreach ($administradores as $admin): ?>
                    <div class="form-group">
                        <label>Nome</label>
                        <input value="<?= $admin ['NOME'] ?>" name="nome" placeholder="Nome" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>CPF</label>
                        <input value="<?= $admin ['CPF'] ?>" name="cpf" placeholder="Nome" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>RG</label>
                        <input value="<?= $admin ['RG'] ?>" name="rg" placeholder="RG" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Telefone¹</label>
                        <input value="<?= $admin ['TELEFONE'] ?>" name="telefone" placeholder="Telefone1" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Telefone²</label>
                        <input value="<?= $admin ['TELEFONE2'] ?>" name="telefone2" placeholder="Telefone²" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Endereço</label>
                        <input value="<?= $admin ['ENDERECO'] ?>" name="endereco" placeholder="Endereço" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input value="<?= $admin ['EMAIL'] ?>" name="email" placeholder="Email" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Login</label>
                        <input value="<?= $admin ['LOGIN'] ?>" name="login" placeholder="Login" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                        <label>Senha</label>
                        <input  name="senha" id="exampleInputPassword" class="form-control"  placeholder="Senha" type="password">
                    </div>
                <?php endforeach; ?>
                <button type="submit" class="btn btn-primary"> Cadastrar</button>
            </form>
		</div>
	</div>
</div>
