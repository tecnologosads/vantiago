<li>
	<a href="javascript: void(0);"><i class="fi-disc"></i><span class="badge badge-info pull-right">10</span> <span> Forms </span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="form-elements.html">Form Elements</a></li>
		<li><a href="form-advanced.html">Form Advanced</a></li>
		<li><a href="form-validation.html">Form Validation</a></li>
		<li><a href="form-pickers.html">Form Pickers</a></li>
		<li><a href="form-wizard.html">Form Wizard</a></li>
		<li><a href="form-mask.html">Form Masks</a></li>
		<li><a href="form-summernote.html">Summernote</a></li>
		<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>
		<li><a href="form-x-editable.html">X Editable</a></li>
		<li><a href="form-uploads.html">Multiple File Upload</a></li>
	</ul>
</li>
