<div class="col-lg-6">

	<div class="card-box">
		<h4 class="header-title m-t-0">Basic Form</h4>
		<p class="text-muted font-14 m-b-20">
			Parsley is a javascript form validation library. It helps you provide your users with feedback on their form submission before sending it to your server.
		</p>

		<form action="#">
			<div class="form-group">
				<label for="userName">User Name<span class="text-danger">*</span></label>
				<input type="text" name="nick" parsley-trigger="change" required
					   placeholder="Enter user name" class="form-control" id="userName">
			</div>
			<div class="form-group">
				<label for="emailAddress">Email address<span class="text-danger">*</span></label>
				<input type="email" name="email" parsley-trigger="change" required
					   placeholder="Enter email" class="form-control" id="emailAddress">
			</div>
			<div class="form-group">
				<label for="pass1">Password<span class="text-danger">*</span></label>
				<input id="pass1" type="password" placeholder="Password" required
					   class="form-control">
			</div>
			<div class="form-group">
				<label for="passWord2">Confirm Password <span class="text-danger">*</span></label>
				<input data-parsley-equalto="#pass1" type="password" required
					   placeholder="Password" class="form-control" id="passWord2">
			</div>
			<div class="form-group">
				<div class="checkbox checkbox-purple">
					<input id="checkbox6a" type="checkbox">
					<label for="checkbox6a">
						Remember me
					</label>
				</div>

			</div>

			<div class="form-group text-right m-b-0">
				<button class="btn btn-custom waves-effect waves-light" type="submit">
					Submit
				</button>
				<button type="reset" class="btn btn-light waves-effect m-l-5">
					Cancel
				</button>
			</div>

		</form>
	</div> <!-- end card-box -->
</div>
