<div class="col-lg-6">
	<div class="card-box">
		<h4 class="header-title m-t-0">Inscreva-se</h4>
		<p class="text-muted font-14 m-b-20">
			Sua inscrição é muito importante para nós. Todos os seus dados estarão seguro com a gente, só dados necessários serão expostos para outros usuários.
		</p>

		<form class="" action="#">
			<div class="form-group">
				<label>Nome</label>
				<input type="text" class="form-control" required
					   placeholder="Nome e sobrenome"/>
			</div>

			<div class="form-group">
				<label>Senha</label>
				<div>
					<input type="password" id="pass2" class="form-control" required
						   placeholder="Senha"/>
				</div>
				<div class="mt-2">
					<input type="password" class="form-control" required
						   data-parsley-equalto="#pass2"
						   placeholder="Digite novamente a sua senha"/>
				</div>
			</div>

			<div class="form-group">
				<label>E-Mail</label>
				<div>
					<input type="email" class="form-control" required
						   parsley-type="email" placeholder="Entre com seu e-mail"/>
				</div>
			</div>
			<div class="form-group">
				<label>Contrato</label>
				<div>
					<!--parsley-type="url"-->
					<input type="text" class="form-control"
						   required placeholder="Contrato"/>
				</div>
			</div>
			<div class="form-group">
				<label>Enderenço</label>
				<div>
					<input data-parsley-type="digits" type="text"
						   class="form-control" required
						   placeholder="Endereço"/>
				</div>
			</div>
			<div class="form-group">
				<label>RG</label>
				<div>
					<input data-parsley-type="number" type="text"
						   class="form-control" required
						   placeholder="RG"/>
				</div>
			</div>
			<div class="form-group">
				<label>Contato</label>
				<div>
					<input data-parsley-type="alphanum" type="text"
						   class="form-control" required
						   placeholder="Contato"/>
				</div>
			</div>
			<div class="form-group">
				<label>CNPJ</label>
				<div>
					<input data-parsley-type="alphanum" type="text"
						   class="form-control" required
						   placeholder="CNPJ"/>
				</div>
			</div>
<!--			<div class="form-group">-->
<!--				<label>Textarea</label>-->
<!--				<div>-->
<!--					<textarea required class="form-control"></textarea>-->
<!--				</div>-->
<!--			</div>-->
			<div class="form-group">
				<div>
					<button type="submit" class="btn btn-custom waves-effect waves-light">
						Inscrever-se
					</button>
					<button type="reset" class="btn btn-light waves-effect m-l-5">
						Cancelar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
