<div class="col-lg-6">
	<div class="card-box">
		<h4 class="header-title m-t-0">Range validation</h4>
		<p class="text-muted font-14 m-b-20">
			Parsley is a javascript form validation library. It helps you provide your users with feedback on their form submission before sending it to your server.
		</p>

		<form action="#">

			<div class="form-group">
				<label>Min Length</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-minlength="6" placeholder="Min 6 chars."/>
				</div>
			</div>
			<div class="form-group">
				<label>Max Length</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-maxlength="6" placeholder="Max 6 chars."/>
				</div>
			</div>
			<div class="form-group">
				<label>Range Length</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-length="[5,10]"
						   placeholder="Text between 5 - 10 chars length"/>
				</div>
			</div>
			<div class="form-group">
				<label>Min Value</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-min="6" placeholder="Min value is 6"/>
				</div>
			</div>
			<div class="form-group">
				<label>Max Value</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-max="6" placeholder="Max value is 6"/>
				</div>
			</div>
			<div class="form-group">
				<label>Range Value</label>
				<div>
					<input class="form-control" required type="text" min="6"
						   max="100" placeholder="Number between 6 - 100"/>
				</div>
			</div>
			<div class="form-group">
				<label>Regular Exp</label>
				<div>
					<input type="text" class="form-control" required
						   data-parsley-pattern="#[A-Fa-f0-9]{6}"
						   placeholder="Hex. Color"/>
				</div>
			</div>

			<div class="form-group m-b-0">
				<div>
					<button type="submit" class="btn btn-custom waves-effect waves-light">
						Submit
					</button>
					<button type="reset" class="btn btn-light waves-effect m-l-5">
						Cancel
					</button>
				</div>
			</div>
		</form>

	</div> <!-- end card-box -->
</div>
