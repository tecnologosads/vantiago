<li>
	<a href="javascript: void(0);"><i class="fi-box"></i><span> Icons </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="icons-materialdesign.html">Material Design</a></li>
		<li><a href="icons-dripicons.html">Dripicons</a></li>
		<li><a href="icons-fontawesome.html">Font awesome</a></li>
		<li><a href="icons-feather.html">Feather Icons</a></li>
		<li><a href="icons-simpleline.html">Simple Line Icons</a></li>
	</ul>
</li>
