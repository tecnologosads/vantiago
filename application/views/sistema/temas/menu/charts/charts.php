<li>
	<a href="javascript: void(0);"><i class="fi-bar-graph-2"></i><span> Charts </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="chart-flot.html">Flot Chart</a></li>
		<li><a href="chart-morris.html">Morris Chart</a></li>
		<li><a href="chart-google.html">Google Chart</a></li>
		<li><a href="chart-chartist.html">Chartist Chart</a></li>
		<li><a href="chart-chartjs.html">Chartjs Chart</a></li>
		<li><a href="chart-sparkline.html">Sparkline Chart</a></li>
		<li><a href="chart-knob.html">Jquery Knob</a></li>
	</ul>
</li>
