<li>
	<a href="javascript: void(0);"><i class="fi-briefcase"></i> <span> UI Elements </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="ui-typography.html">Typography</a></li>
		<li><a href="ui-cards.html">Cards</a></li>
		<li><a href="ui-buttons.html">Buttons</a></li>
		<li><a href="ui-modals.html">Modals</a></li>
		<li><a href="ui-spinners.html">Spinners</a></li>
		<li><a href="ui-ribbons.html">Ribbons</a></li>
		<li><a href="ui-tooltips-popovers.html">Tooltips & Popover</a></li>
		<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>
		<li><a href="ui-tabs.html">Tabs</a></li>
		<li><a href="ui-progressbars.html">Progress Bars</a></li>
		<li><a href="ui-notifications.html">Notification</a></li>
		<li><a href="ui-grid.html">Grid</a></li>
		<li><a href="ui-sweet-alert.html">Sweet Alert</a></li>
		<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>
		<li><a href="ui-range-slider.html">Range Slider</a></li>
	</ul>
</li>
