<li>
	<a href="javascript: void(0);"><i class="fi-paper"></i> <span> Tables </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="tables-basic.html">Basic Tables</a></li>
		<li><a href="tables-datatable.html">Data Tables</a></li>
		<li><a href="tables-responsive.html">Responsive Table</a></li>
		<li><a href="tables-tablesaw.html">Tablesaw Tables</a></li>
		<li><a href="tables-foo.html">Foo Tables</a></li>
	</ul>
</li>
