<li>
	<a href="javascript: void(0);"><i class="fi-paper-stack"></i><span> Pages </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="page-starter.html">Starter Page</a></li>
		<li><a href="page-login.html">Login</a></li>
		<li><a href="page-register.html">Register</a></li>
		<li><a href="page-logout.html">Logout</a></li>
		<li><a href="page-recoverpw.html">Recover Password</a></li>
		<li><a href="page-lock-screen.html">Lock Screen</a></li>
		<li><a href="page-confirm-mail.html">Confirm Mail</a></li>
		<li><a href="page-404.html">Error 404</a></li>
		<li><a href="page-404-alt.html">Error 404-alt</a></li>
		<li><a href="page-500.html">Error 500</a></li>
	</ul>
</li>
