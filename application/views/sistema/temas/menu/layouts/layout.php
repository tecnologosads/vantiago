<li>
	<a href="javascript: void(0);"><i class="fi-layout"></i><span> Layouts </span> <span class="menu-arrow"></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="layouts-menucollapsed.html">Menu Collapsed</a></li>
		<li><a href="layouts-small-menu.html">Small Menu</a></li>
		<li><a href="layouts-dark-lefbar.html">Dark Leftbar</a></li>
		<li><a href="layouts-center-logo.html">Center Logo</a></li>
	</ul>
</li>
