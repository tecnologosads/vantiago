<li>
	<a href="javascript:void(0);"><i class="fi-marquee-plus"></i><span class="badge badge-success pull-right">Hot</span> <span> Extra Pages </span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li><a href="extras-timeline.html">Timeline</a></li>
		<li><a href="extras-profile.html">Profile</a></li>
		<li><a href="extras-invoice.html">Invoice</a></li>
		<li><a href="extras-faq.html">FAQ</a></li>
		<li><a href="extras-pricing.html">Pricing</a></li>
		<li><a href="extras-email-template.html">Email Templates</a></li>
		<li><a href="extras-ratings.html">Ratings</a></li>
		<li><a href="extras-search-results.html">Search Results</a></li>
		<li><a href="extras-gallery.html">Gallery</a></li>
		<li><a href="extras-maintenance.html">Maintenance</a></li>
		<li><a href="extras-coming-soon.html">Coming Soon</a></li>
	</ul>
</li>
