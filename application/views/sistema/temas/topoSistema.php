<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Highdmin - Responsive Bootstrap 4 Admin Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- App favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<!-- App css -->
	<?php $this->load->helper("csssistema_helper") ?>

	<?php $this->load->helper("jssistematop_helper") ?>

</head>


<body>


<?php if ($this->session->userdata("administrador_logado")) : ?>
    <!-- Begin page -->
    <div id="wrapper">
        <?php $this->load->view("sistema/temas/menu/verticalmenu") ?>

        <div class="content-page">

            <!-- Top Bar Start -->
            <div class="topbar">
                <?php $this->load->view("sistema/temas/navbar") ?>
            </div>
            <!-- Top Bar End -->
<!--            <div class="card-box">-->
<!--                --><?php //$this->load->view("sistema/temas/menu/form/validation/validationtype") ?>
<!--                --><?php //$this->load->view("sistema/temas/menu/form/formelements/login") ?>
<!--            </div>-->
        </div>
    </div>

<?php endif; ?>

<!-- END wrapper -->
	<?php $this->load->helper("jssistemadown_helper") ?>
</body>
</html>
