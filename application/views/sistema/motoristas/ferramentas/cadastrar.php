
<div class="row">
	<div class="col-md-12">
		<a  href="<?php echo base_url(); ?>index.php/Motorista" class="btn btn-primary"><i class=" mdi mdi-keyboard-backspace" ></i>   Voltar </a>
		<div class="card-box">
			<h4 class="m-t-0 header-title">Cadastro de motoristas</h4>
			<p class="text-muted m-b-30 font-13">
				Dados obrigátorios <span class="text-danger">*</span>
			</p>

			<form method="post" action="<?php echo base_url();?>Index.php/Motorista/create">
				<div class="form-row">
<!--					nome-->
					<div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Nome<span class="text-danger">*</span></label>
						<input required type="text" minlength="4" name="NOME" class="form-control" id="inputEmail4" placeholder="Nome">
					</div>

<!--					CPF-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">CPF<span class="text-danger">*</span></label>
						<input  maxlength="11" name="CPF" required="" placeholder="CPF" class="form-control cpf">
					</div>

<!--					RG-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">RG<span class="text-danger">*</span></label>
						<input  maxlength="11" name="RG" required="" placeholder="RG" class="form-control rg">
					</div>

<!--					Telefone-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Telefone<span class="text-danger">*</span></label>
						<input required minlength="16" name="TELEFONE" class="form-control phone" id="inputEmail4" placeholder="Entre com seu telefone">
					</div>

<!--					telefone 2-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Telefone²</label>
						<input minlength="16" name="TELEFONE2" class="form-control phone"id="inputEmail4" placeholder="Entre com seu telefone">
					</div>

<!--					CNH-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">CNH<span class="text-danger">*</span></label>
						<input minlength="11" required name="CNH" class="form-control cnh" id="inputEmail4" placeholder="Entre com seu telefone">
					</div>

					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Validade CNH<span class="text-danger">*</span></label>
						<input type="date" required name="VALIDADE_CNH" class="form-control" id="inputEmail4" placeholder="Entre com seu telefone">
					</div>


				</div>
<!--				Endereço-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Endereço<span class="text-danger">*</span></label>
					<input name="ENDERECO" required minlength="20" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
				</div>

<!--				Email-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Email<span class="text-danger">*</span></label>
					<input name="EMAIL" required minlength="10" type="email" class="form-control" id="inputAddress" placeholder="Entre com seu email">
				</div>


				<button type="submit" class="btn btn-primary"><i class="mdi mdi-check"></i>   Cadastrar</button>
				<button type="reset" id="tooltip-animation" class="btn btn-primary" title="Atenção, isso vai apagar todos os campos preenchidos"><i class=" mdi mdi-delete-empty "></i> Limpar o formulário</button>
			</form>
		</div>
	</div>
</div>
