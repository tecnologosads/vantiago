<a href="<?php echo base_url(); ?>index.php/Motorista/carregarCadastro"  type="button" class="btn btn-primary waves-light waves-effect">Novo <i class="mdi mdi-account-plus"></i></a>
<div class="row">
                <div class="col-lg-10">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title">Lista de Motorista</h4>
                        <table class="table">
                            <thead>
                            <tr>
								<th>Nome</th>
								<th>CPF</th>
								<th>RG</th>
								<th>CNH</th>
								<th>Telefone</th>
								<th>Telefone²</th>
								<th>Endereço</th>
								<th>Email</th>
								<th><i class="dripicons-trash"></i></th>
								<th><i class="dripicons-pencil"></i></th>
							</tr>
                            </thead>
                            <tbody>
                            <?php foreach ($motoristas as $motorista): ?>
                                <tr>
									<th scope="row"><a href="<?php echo base_url(); ?>index.php/Motorista/carregarPerfil/<?php echo $motorista->ID ?>"><?php echo $motorista->NOME?></a></th>
                                    <th scope="row"><?php echo $motorista->CPF?></th>
                                    <th scope="row"><?php echo $motorista->RG?></th>
                                    <th scope="row"><?php echo $motorista->CNH?></th>
                                    <th scope="row"><?php echo $motorista->TELEFONE?></th>
                                    <th scope="row"><?php echo $motorista->TELEFONE2?></th>
									<th scope="row"><?php echo $motorista->ENDERECO?></th>
									<th scope="row"><?php echo $motorista->EMAIL?></th>
									<td><a href="<?php echo base_url(); ?>index.php/Motorista/delete/<?php echo $motorista->ID ?> "><i
												class="dripicons-trash"></i></a></td>
									<td><a href="<?php echo base_url(); ?>index.php/Motorista/updateget/<?php echo $motorista->ID ?>"><i
												class="dripicons-pencil"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>

</div>
