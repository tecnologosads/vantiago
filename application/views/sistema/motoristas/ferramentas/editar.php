<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>index.php/Motorista" class="btn btn-primary"><i class=" mdi mdi-keyboard-backspace" ></i>   Voltar</a>
		<div class="card-box">
			<h4 class="m-t-0 header-title">Atualização das informações do(a) motorista: <?php echo $update->NOME?></h4>
			<p class="text-muted m-b-30 font-13">
				Dados obrigátorios <span class="text-danger">*</span>
			</p>

			<form method="post" action="<?php echo base_url();?>Index.php/Motorista/updateset">
				<input type="hidden" name="id" value="<?php echo $update->ID?>">
				<div class="form-row">
					<!--					nome-->
					<div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Nome<span class="text-danger">*</span></label>
						<input type="text" minlength="4" required name="NOME" value="<?php echo $update->NOME?>" class="form-control" id="inputEmail4" placeholder="Nome">
					</div>

					<!--					CPF-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">CPF<span class="text-danger">*</span></label>
						<input type="text" minlength="14" required  name="CPF" value="<?php echo $update->CPF ?>" class="form-control" id="inputEmail4" placeholder="Entre com seu cpf">
					</div>

					<!--					RG-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">RG<span class="text-danger">*</span></label>
						<input type="text" minlength="8" required  name="RG" value="<?php echo $update->RG ?>" class="form-control" id="inputEmail4" placeholder="Entre com seu rg">
					</div>

					<!--					Telefone-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Telefone<span class="text-danger">*</span></label>
						<input type="text" minlength="16" required  name="TELEFONE" value="<?php echo $update->TELEFONE ?>" class="form-control" id="inputEmail4" placeholder="Entre com seu telefone">
					</div>

					<!--					telefone 2-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Telefone²</label>
						<input type="text" minlength="16" name="TELEFONE2" value="<?php echo $update->TELEFONE2?>" class="form-control" id="inputEmail4" placeholder="Entre com seu telefone">
					</div>

				</div>
				<!--				Endereço-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Endereço<span class="text-danger">*</span></label>
					<input name="ENDERECO" required minlength="20" type="text" value="<?php echo $update->ENDERECO?>" class="form-control" id="inputAddress" placeholder="1234 Main St">
				</div>

				<!--				Email-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Email<span class="text-danger">*</span></label>
					<input name="EMAIL" required minlength="20" value="<?php echo $update->EMAIL ?>" type="email" class="form-control" id="inputAddress" placeholder="Entre com seu email">
				</div>

				<!--				Senha-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Senha<span class="text-danger">*</span></label>
					<input name="SENHA" required minlength="8" value="<?php echo md5($update->SENHA); ?>" type="password" class="form-control" id="inputAddress" placeholder="Entre com seu email">
				</div>

				<button type="submit" class="btn btn-primary"><i class=" mdi mdi-autorenew"></i>   Atualizar</button>
				<button type="reset" id="tooltip-animation" class="btn btn-primary" title="Atenção, isso vai apagar todos os campos preenchidos"><i class=" mdi mdi-delete-empty "></i> Limpar o formulário</button>
			</form>
		</div>
	</div>
</div>
