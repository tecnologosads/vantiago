<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- App favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<!-- App css -->
	<?php $this->load->helper("csssistema_helper"); ?>


	<?php $this->load->helper("jssistematop_helper"); ?>

</head>


<body>

<!-- Begin page -->
<div id="wrapper">

	<!-- ========== Left Sidebar Start ========== -->
	<?php $this->load->view("sistema/htmlelements/verticalmenu"); ?>
	<!-- Left Sidebar End -->



	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->

	<div class="content-page">

		<!-- Start Page content -->
		<div class="content">
			<div class="container-fluid">
				<?php $this->load->view("sistema/dashboard/dashboard-demo"); ?>
			</div> <!-- container -->

		</div> <!-- content -->

		<footer class="footer text-right">
			2018 © Highdmin. - Coderthemes.com
		</footer>

	</div>


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->


</div>
<?php $this->load->helper("jssistemadown_helper"); ?>
</body>
</html>
