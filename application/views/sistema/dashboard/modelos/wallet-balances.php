<div class="row">
	<div class="col-lg-10">
		<div class="card-box">
			<h4 class="header-title mb-3">Veiculos</h4>

			<div class="table-responsive">
				<table class="table">
					<thead>
					<tr>
						<th>ANO</th>
						<th>VAGAS</th>
						<th>PLACA</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($proprietarios as $proprietario): ?>
						<tr>
							<th scope="row"><?php echo $proprietario->ANO?></th>
							<th scope="row"><?php echo $proprietario->QTD_LUGARES?></th>
							<th scope="row"><?php echo $proprietario->PLACA?></th>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				</table>
			</div>
		</div>

	</div>

</div>
