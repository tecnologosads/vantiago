<div class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-12">
				<div class="card-box">
					<h4 class="header-title mb-4">Informações da conta do(a): <?php echo $this->session->userdata('nome')  ?></h4>

					<div class="row">
						<!--					Dinheiro ganho-->
						<div class="col-sm-6 col-lg-6 col-xl-3">
							<div class="card-box mb-0 widget-chart-two">
								<div class="float-right">Pagamentos efetuados
									<input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
										   data-fgColor="#0acf97" value="3" data-skin="tron" data-angleOffset="360"
										   data-readOnly=true data-thickness=".1"/>
								</div>
								<div class="widget-chart-two-content">
									<p class="text-muted mb-0 mt-2">Ganhos do mês</p>
									<h3 class="">R$350,00</h3>
								</div>

							</div>
						</div>

<!--						Gastos-->
						<div class="col-sm-6 col-lg-6 col-xl-3">
							<div class="card-box mb-0 widget-chart-two">
								<div class="float-right">Vezes gastos
									<input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
										   data-fgColor="#f1556c" value="6" data-skin="tron" data-angleOffset="180"
										   data-readOnly=true data-thickness=".1"/>
								</div>
								<div class="widget-chart-two-content">
									<p class="text-muted mb-0 mt-2">Gastos</p>
									<h3 class="">R$600,00</h3>
								</div>

							</div>
						</div>

<!--						Gasto com gasolina-->
						<div class="col-sm-6 col-lg-6 col-xl-3">
							<div class="card-box mb-0 widget-chart-two">
								<div class="float-right">Vezes abastecido
									<input data-plugin="knob" data-width="80" data-height="80" data-linecap=round
										   data-fgColor="#2d7bf4" value="5" data-skin="tron" data-angleOffset="180"
										   data-readOnly=true data-thickness=".1"/>
								</div>
								<div class="widget-chart-two-content">
									<p class="text-muted mb-0 mt-2">Total gasto com combustível</p>
									<h3 class="">R$500,00</h3>
								</div>

							</div>
						</div>
					</div>
					<!-- end row -->
				</div>
			</div>
		</div>
		<!-- end row -->

		<div class="row">
			<div class="col-lg-8">
				<div class="card-box">
					<h4 class="header-title mb-3">Passageiros</h4>

					<div class="table-responsive">
						<table class="table table-hover table-centered m-0">

							<thead>
							<tr>
								<th>Turno</th>
								<th>Nome</th>
								<th>Colegio</th>
								<th>Pagamento</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<h5 class="m-0 font-weight-normal">Noturno</h5>
								</td>

								<td>
									<h5 class="m-0 font-weight-normal">Anderson Andrade</h5>
									<p class="mb-0 text-muted"><small>Membro desde 2017</small></p>
								</td>

								<td>
									 Unidesc
								</td>

								<td>
									Efetuado
								</td>
							</tr>

							<tr>
								<td>
									<h5 class="m-0 font-weight-normal">Matutino</h5>
								</td>

								<td>
									<h5 class="m-0 font-weight-normal">Tiago Silva</h5>
									<p class="mb-0 text-muted"><small>Membro desde 2018</small></p>
								</td>

								<td>
									Unidesc
								</td>

								<td>
									Efetuado
								</td>
							</tr>

							<tr>
								<td>
									<h5 class="m-0 font-weight-normal">Vespertino</h5>
								</td>

								<td>
									<h5 class="m-0 font-weight-normal">José Santos</h5>
									<p class="mb-0 text-muted"><small>Membro desde 2019</small></p>
								</td>

								<td>
									Unidesc
								</td>

								<td>
									Pendente
								</td>
							</tr>

							</tbody>
						</table>
					</div>
				</div>

			</div>

		</div>
		<!-- end row -->

	</div> <!-- container -->

</div> <!-- content -->
