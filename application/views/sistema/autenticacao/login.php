<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <?php $this->load->helper("csssistema_helper"); ?>
    <?php $this->load->helper("jssistematop_helper"); ?>


</head>


<body class="account-pages">
<div id="mensagem"></div>

<!-- Begin page -->
<div class="accountbg background_login"
     style="background-image: url('<?php echo base_url(); ?>/assets/sistema/images/home/background_home.jpg')"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">
            <div class="account-box">
                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="index.html" class="text-success">
                            <span><img src="<?php echo base_url() ?>/assets/sistema/images/home/logo_header.png" alt=""></span>
                        </a>
                    </h2>
                    <form class="" id="formulario_cadastro" method="post"
                          action="<?php echo base_url(); ?>index.php/login/autenticar">

                        <div class="form-group m-b-20 row">
                            <div class="col-12">
                                <label for="emailaddress">Email</label>
                                <input name="login" class="form-control" type="email" id="email" required=""
                                       placeholder="Digite seu email">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="password">Senha</label>
                                <input name="senha" class="form-control" type="password" required="" id="senha"
                                       placeholder="Digite sua senha">
                                <a href="page-recoverpw.html" class="text-muted pull-right">
                                    <small>Esqueceu sua senha?</small>
                                </a>
                            </div>
                        </div>

                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Entrar
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row m-t-50">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Não possui uma conta? <a href="page-register.html"
                                                                           class="text-dark m-l-5"><b>Cadastre-se</b></a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="m-t-40 text-center">
        <p class="account-copyright">2019 - TADS - Gerenciamento de frota escolar</p>
    </div>

</div>
<?php $this->load->helper("jssistemadown_helper"); ?>
</body>
</html>


<?php if ($this->session->flashdata("danger")) : ?>
    <script>
        $('.mensagem_erro_login').ready(function () {
                swal(
                    "Erro", "Usuário ou senha incorretos!", "error"
                )
            }
        );
    </script>
<?php endif; ?>

