<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h1 class="m-t-0">Cadastro de Alunos </h1>
            <form role="form" method="post" novalidate="" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/aluno/create">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <?php $this->load->view("sistema/htmlelements/botaoAdicionarFoto2.html"); ?>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Van do aluno</label>
                        <select name="id_veiculo" class="form-control">

                            <?php foreach ($vans as $van): ?>
                                <option value="<?= $van-> ID ?>"><?= $van-> MODELO ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Nome<span class="text-danger">*</span></label>
                        <input name="nome" placeholder="Nome" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Data de Nascimento<span class="text-danger">*</span></label>
                        <input name="data_nascimento" required="" type="date"  class="form-control">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>CPF<span class="text-danger">*</span></label>
                        <input  maxlength="11" name="cpf" required="" placeholder="CPF" class="form-control cpf">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Telefone¹<span class="text-danger">*</span></label>
                        <input name="telefone" required="" placeholder="Telefone1" class="form-control phone">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Endereço<span class="text-danger">*</span></label>
                        <input name="endereco" required="" placeholder="Endereço" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Cidade<span class="text-danger">*</span></label>
                        <input name="cidade" required="" placeholder="Cidade" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Bairro</label>
                        <input name="bairro" placeholder="Bairro" class="form-control">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail3">Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" required="" parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                </div>

                <div class="form-group row float-right">
                    <button type="submit" class=" btn btn-info waves-effect waves-light"> <i class="fa mdi mdi-content-save m-r-5"></i> <span>Cadastrar</span> </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar e recortar imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe class="iframe_carregar_imagem" src="<?= base_url() ?>index.php/base"></iframe>
            </div>

        </div>
    </div>
</div>
