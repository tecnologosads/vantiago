
<div class="row">
	<div class="card-box table-responsive">
		<div class="col-12">
            <h1 class="m-t-0">Alunos</h1>
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0"
                   width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
                <thead>
                <tr role="row">
                    <th>Veículo</th>
                    <th>Imagem</th>
                    <th>#</th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Telefone</th>
                    <th>CIDADE</th>
                    <th>BAIRRO</th>
                    <th><i class="dripicons-trash"></i></th>
                    <th><i class="dripicons-pencil"></i></th>
                </tr>
                </thead>


                <?php


                foreach ($alunos  as $aluno):

                    ?>

                <tbody>

					<tr role="row" class="odd">
                        <td class="sorting_1"><?= $aluno->MODELO ?></td>
                        <td class="sorting_1"><img class="imagem_aluno" src="<?=  $diretorio = base_url()."assets/sistema/images/imagens_pessoas/$aluno->CPF" . ".jpg";?> "></td>
                        <td class="sorting_1"><?php echo $aluno->ID ?></td>
						<td class="sorting_1"><?php echo $aluno->NOME ?></td>
						<td class="sorting_1"><?php echo $aluno->CPF ?></td>
						<td class="sorting_1"><?php echo $aluno->TELEFONE ?></td>
						<td class="sorting_1"><?php echo $aluno->CIDADE ?></td>
						<td class="sorting_1"><?php echo $aluno->BAIRRO ?></td>
						<td><a href="<?php echo base_url(); ?>index.php/aluno/delete/<?php echo $aluno->ID ?>"><i
									class="dripicons-trash"></i></a></td>
						<td><a href="<?php echo base_url(); ?>index.php/aluno/update/<?php echo $aluno->ID ?>"><i
									class="dripicons-pencil"></i></a></td>
					</tr>
                </tbody>

                <?php endforeach; ?>

			</table>
		</div>
	</div>
</div>
</div>
</div>
</div>
