<div class="topbar-left">
    <a href="index.html" class="logo">
        <span>
            <img src="<?= base_url() ?>/assets/sistema/images/home/logo_header.png" alt="" height="40">
        </span>
    </a>
</div>
<div class="user-box">
    <div class="user-img">
        <img src="<?= base_url() ?>assets/sistema/images/imagens_pessoas/<?= $this->session->userdata('cpf') ?>.jpg"
             alt="user-img"" class="rounded-circle img-fluid">
    </div>
    <h5><a href="#"><?= $this->session->userdata('nome') ?></a></h5>
    <p class="text-muted">ID: <?= $this->session->userdata('id') ?></p>
</div>