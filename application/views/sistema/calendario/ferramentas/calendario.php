<div class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="card-box">
					<div class="row">
						<div class="col-lg-3">
							<a href="#" data-toggle="modal" data-target="#add-category" class="btn btn-lg btn-custom btn-block waves-effect m-t-20 waves-light">
								<i class="fi-circle-plus"></i> Criar novo evento
							</a>
							<div id="external-events" class="m-t-20">
								<br>
								<p class="text-muted">Mova o evento para o dia do calendário</p>
								<div class="external-event bg-success" data-class="bg-success">
									<i class="mdi mdi-checkbox-blank-circle mr-2 vertical-middle"></i>Salário
								</div>

							</div>

							<!-- checkbox -->
							<div class="checkbox checkbox-primary mt-3">
								<input type="checkbox" id="drop-remove">
								<label for="drop-remove">
									Remover após adicionar
								</label>
							</div>

							<div class="mt-5 d-none d-xl-block">
								<h5 class="text-center">Como funciona ?</h5>

								<ul class="pl-3">
									<li class="text-muted mb-3">
										Você pode criar todos os eventos marcando cada um deles com uma cord diferente, para que assim possa ter uma maior organização.
									</li>
									<li class="text-muted mb-3">
										Você pode marcar o tempo em que cada evento vai durar, tornando assim mais fácil para saber quando pode marcar novos eventos ou não.
									</li>
									<li class="text-muted mb-3">
										Após criar um evento você tem a opção "remover após adicionar" o que permite que ele seja adicionado apenas uma véz, quando essa
										opção está desmarcada ela você pode adicionar o mesmo evento várias vezes.
									</li>
								</ul>
							</div>
						</div> <!-- end col-->
						<div class="col-lg-9">
							<div id="calendar"></div>
						</div> <!-- end col -->
					</div>  <!-- end row -->
				</div>

				<!-- BEGIN MODAL -->
				<div class="modal fade" id="event-modal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header text-center border-bottom-0 d-block">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Add New Event</h4>
							</div>
							<div class="modal-body">

							</div>
							<div class="modal-footer border-0 pt-0">
								<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
								<button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal Add Category -->
				<div class="modal fade" id="add-category" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header text-center border-bottom-0 d-block">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title mt-2">Add a category</h4>
							</div>
							<div class="modal-body p-4">
								<form role="form">
									<div class="form-group">
										<label class="control-label">Category Name</label>
										<input class="form-control form-white" placeholder="Enter name" type="text" name="category-name"/>
									</div>
									<div class="form-group">
										<label class="control-label">Choose Category Color</label>
										<select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
											<option value="success">Success</option>
											<option value="danger">Danger</option>
											<option value="info">Info</option>
											<option value="pink">Pink</option>
											<option value="primary">Primary</option>
											<option value="warning">Warning</option>
											<option value="inverse">Inverse</option>
										</select>
									</div>

								</form>

								<div class="text-right">
									<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-custom ml-1 waves-effect waves-light save-category" data-dismiss="modal">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MODAL -->
			</div>
			<!-- end col-12 -->
		</div> <!-- end row -->

	</div> <!-- container -->

</div> <!-- content -->
