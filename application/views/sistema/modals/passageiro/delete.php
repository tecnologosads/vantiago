<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 3/12/2019
 * Time: 4:55 PM
 */
?>
<div id="custom-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span><span class="sr-only">Close</span>
	</button>
	<h4 class="custom-modal-title">Confirmação de delete</h4>
	<div class="custom-modal-text">
		<form class="form-horizontal" action="#">

			<div class="form-group m-b-25">
				<div class="col-12">
					<label for="emailaddress3">Tem certeza que deseja excluir essa informação?</label>
					<a href="<?php echo base_url(); ?>index.php/Passageiro/delete/<?php echo $passageiros->id ?>"><i class="dripicons-trash">deletar</i></a>
				</div>
			</div>
		</form>
	</div>
</div>

