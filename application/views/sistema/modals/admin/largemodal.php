<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>

<a href="<?php echo base_url(); ?>index.php/admin/loadcreate" class="btn float-right btn-custom waves-light waves-effect">
    Novo
    <i class="fa mdi mdi-account-plus m-r-5"></i>
</a>

