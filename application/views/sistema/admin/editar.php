<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h1 class="m-t-0">Editar Administrador</h1>
            <form role="form" method="post" novalidate="" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/admin/updatesave">
                <input type="hidden" name="idsave" value="<?php echo $updates->ID?>">

                <div class="form-row">
                    <div class="form-group col-md-2">
                      <?php
                        $diretorio = base_url()."assets/sistema/images/imagens_pessoas/$updates->CPF" . ".jpg";
                      ?>
                        <?php echo "<img class='imagem_pessoa img-fluid img-thumbnail' src='$diretorio'>"; ?>
                    </div>
                    <div class="form-group col-md-2">
                        <?php $this->load->view("sistema/htmlelements/botaoAdicionarFoto2.html"); ?>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Nome<span class="text-danger">*</span></label>
                        <input name="nome" value="<?php echo $updates->NOME?>" placeholder="Nome" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-2">
                        <label>Data de Nascimento<span class="text-danger">*</span></label>
                        <input name="data_nascimento" value="<?php echo $updates->DATA_NASCIMENTO?>" required="" type="date"  class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Genero</label>
                        <select name="genero" class="form-control">
                            <option value="?">Prefiro não informar</option>
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                            <option value="O">Outros</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>CPF<span class="text-danger">*</span></label>
                        <input  maxlength="11" value="<?php echo $updates->CPF?>" name="cpf" required="" placeholder="Nome" class="form-control cpf">
                    </div>
                    <div class="form-group col-md-6">
                        <label>RG<span class="text-danger">*</span></label>
                        <input name="rg" VALUE="<?php echo $updates->RG?>" data-parsley-type="number" placeholder="RG" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Telefone¹<span class="text-danger">*</span></label>
                        <input name="telefone" VALUE="<?php echo $updates->TELEFONE?>" required="" placeholder="Telefone1" class="form-control phone">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Telefone²</label>
                        <input name="telefone2" VALUE="<?php echo $updates->TELEFONE2?>" type="tel" placeholder="Telefone²" class="form-control phone">
                    </div>
                    <div class="form-group col-md-4">
                        <label>CEP</label>
                        <input maxlength="8" VALUE="<?php echo $updates->CEP?>" name="cep" placeholder="CEP" class="form-control cep">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Endereço<span class="text-danger">*</span></label>
                        <input name="endereco" VALUE="<?php echo $updates->ENDERECO?>" required="" placeholder="Endereço" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Cidade<span class="text-danger">*</span></label>
                        <input name="cidade" VALUE="<?php echo $updates->CIDADE?>" required="" placeholder="Cidade" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Bairro</label>
                        <input name="bairro" VALUE="<?php echo $updates->BAIRRO?>" placeholder="Bairro" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail3">Email <span class="text-danger">*</span></label>
                        <input type="email" VALUE="<?php echo $updates->EMAIL?>" name="email" required="" parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="inputEmail3">Senha <span class="text-danger">*</span></label>
                        <input id="hori-pass1" type="password" placeholder="Senha" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="inputEmail3">Confirme a Senha <span class="text-danger">*</span></label>
                        <input data-parsley-equalto="#hori-pass1" type="password" required="" placeholder="Confirme a senha" name="senha" class="form-control" id="hori-pass2">
                    </div>
                </div>
                <div class="form-group row float-right">
                    <button type="submit" class=" btn btn-info waves-effect waves-light"> <i class="fa mdi mdi-content-save m-r-5"></i> <span>Cadastrar</span> </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar e recortar imagem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe class="iframe_carregar_imagem" src="<?= base_url() ?>index.php/base"></iframe>
            </div>

        </div>
    </div>
</div>
