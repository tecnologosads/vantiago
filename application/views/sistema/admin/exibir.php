
<div class="row">
	<div class="card-box table-responsive">
		<div class="col-12">
            <h1 class="m-t-0">Administradores</h1>
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0"
				   width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
				<thead>
				<tr role="row">
					<th>IMAGEM</th>
					<th>#</th>
					<th>Nome</th>
					<th>CPF</th>
					<th>RG</th>
					<th>Telefone</th>
					<th>Endereco</th>
					<th>Email</th>
					<th><i class="dripicons-trash"></i></th>
					<th><i class="dripicons-pencil"></i></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($administradores as $admin): ?>
					<tr role="row" class="odd">
                        <td class="sorting_1"><img class="imagem_aluno" src="<?=  $diretorio = base_url()."assets/sistema/images/imagens_pessoas/$admin->CPF" . ".jpg";?> "></td>
                        <td class="sorting_1"><?php echo $admin-> ID ?></td>
						<td class="sorting_1"><?php echo $admin-> NOME ?></td>
						<td class="sorting_1"><?php echo $admin->CPF ?></td>
						<td class="sorting_1"><?php echo $admin->RG ?></td>
						<td class="sorting_1"><?php echo $admin->TELEFONE ?></td>
						
						<td class="sorting_1"><?php echo $admin->ENDERECO ?></td>
						<td class="sorting_1"><?php echo $admin->EMAIL ?></td>
						<td><a href="<?php echo base_url(); ?>index.php/admin/delete/<?php echo $admin->ID ?>"><i
									class="dripicons-trash"></i></a></td>
						<td><a href="<?php echo base_url(); ?>index.php/admin/update/<?php echo $admin->ID ?>"><i
									class="dripicons-pencil"></i></a></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</div>
</div>
