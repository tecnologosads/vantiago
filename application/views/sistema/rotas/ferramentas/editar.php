<div class="row">
	<div class="col-md-6">
		<div class="card-box">
			<form method="post" action="<?php echo base_url();?>index.php/Rotas/updateset">
				<input type="hidden" name="id" value="<?php echo $update->id?>">

				<?php echo $update->link?>
				<div class="form-group row">
					<label class="col-2 col-form-label">Link do google</label>
					<div class="col-10">
						<textarea name="link" class="form-control" rows="5"><?php echo $update->link?></textarea>
					</div>
				</div>

				<div class="form-group">
					<label>nome</label>
					<input name="nome_rota" placeholder="Teste_test@outlook.com" value="<?php echo $update->nome_rota?>" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
				</div>

                <button type="submit" class="btn btn-primary">Atualizar</button>
                <a href="<?php echo base_url(); ?>index.php/Rotas" class="btn btn-primary">Voltar</a>
            </form>
        </div>
    </div>
</div>
