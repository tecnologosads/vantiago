<div class="row">
    <div class="col-md-6">
        <h4 class="m-t-0 header-title">REGI�ES ATENDIDAS</h4>
        <p class="text-muted font-14">
            Selecione o seu ve�culo, em seguida marque os bairros atendidos por sua van.
        </p>
        
        <form method="post" action="<?php echo base_url();?>index.php/Rotas/create">
            <div class="form-group col-md-12">
                <label>Van</label>
                <select name="id_veiculo" class="form-control">
        
                    <?php foreach ($vans as $van): ?>
                        <option value="<?= $van-> ID ?>"><?= $van-> MODELO ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
    
            <table class="table table-striped">
            	<thead>
                    <tr>
                    	<th>Cidade</th>  
                    	<th>Bairro</th>     
                    <tr>
                <thead>
            	
            	<tbody>
            	<?php
                    $i2 = 1;
                    foreach ($bairros as $b):
                ?>
                	<tr>
                		<th scope="row">
                			<label><?= $b->CIDADE ?></label>
                		</th>
                		
                		<td>
            			<div class="custom-control custom-checkbox bairros">
            				<input type="checkbox" class="custom-control-input" name="bairroVeiculo" value="<?= $b->ID_BAIRRO ?>" id="customCheck<?= $i2 ?>">
                        	<label class="custom-control-label labelCidades" for="customCheck<?= $i2 ?>"><?= $b->BAIRRO ?></label>
            			</div>
            			</td>
            		</tr>
                    
                <?php
                    $i2 ++;
                    endforeach
                    ?>
            	</tbody>	
            </table>
    </div>
    
    
    <div class="col-md-6">
        <h4 class="m-t-0 header-title">LINK DA ROTA</h4>
        <p class="text-muted font-14">
            Copie o cole aqui o link gerado por sua rota no Google My Maps.
        </p>
        <div class="card-box">
                <div class="form-group">
                    <label>Link da rota</label>
                    <input name="link" placeholder="Insira aqui o link" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>

                <div class="form-group">
                    <label>Nome da rota</label>
                    <input name="nome_rota" placeholder="Insira o nome que quer dar a rota" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp">
                </div>
				<button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Manual de cadastro</button>
				<button type="submit" class="btn btn-primary"> Cadastrar</button>
            </form>
        </div>
    </div>
</div>
