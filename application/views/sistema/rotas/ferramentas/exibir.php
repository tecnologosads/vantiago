<a href="<?php echo base_url(); ?>index.php/Rotas/carregarCadastro" class="btn btn-primary waves-light waves-effect">Novo</a>

<div class="row">
                <div class="col-lg-10">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title">Lista de rotas</h4>
                        <table class="table">
                            <thead>
                            <tr>
								<th>Nome da rota</th>
								<th><i class="dripicons-trash"></i></th>
								<th><i class="dripicons-pencil"></i></th>
							</tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rotas as $rota): ?>
                                <tr>
                                    <th scope="row"><?php echo $rota->nome_rota?></th>
									<td><a href="<?php echo base_url(); ?>index.php/Rotas/delete/<?php echo $rota->id ?> "><i
												class="dripicons-trash"></i></a></td>
									<td><a href="<?php echo base_url(); ?>index.php/Rotas/updateget/<?php echo $rota->id ?>"><i
												class="dripicons-pencil"></i></a></td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
		</div>
	</div>

</div>
