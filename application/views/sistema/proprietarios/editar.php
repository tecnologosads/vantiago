<button type="button" class=" btn btn-info btn-primary waves-effect waves-light" onclick="history.go(-1)">
    <i class="fa mdi mdi mdi-arrow-left-thick m-r-5"></i>
    <span>Voltar</span>
</button>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h1 class="m-t-0">Editar Proprietário</h1>
            <form role="form" method="post" novalidate="" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/proprietario/updatesave">
                <input type="hidden" name="idsave" value="<?php echo $updates->ID?>">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <?php $this->load->view("sistema/htmlelements/botaoAdicionarFoto.html"); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Nome<span class="text-danger">*</span></label>
                        <input name="nome" value="<?php echo $updates->NOME?>" placeholder="Nome" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Data de Nascimento<span class="text-danger">*</span></label>
                        <input name="data_nascimento" value="<?php echo $updates->DATA_NASCIMENTO?>" required="" type="date" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Genero</label>
                        <select name="genero" class="form-control">
                            <option value="?">Prefiro não informar</option>
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                            <option value="O">Outros</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>CPF</label>
                        <input maxlength="11" value="<?php echo $updates->CPF?>" name="cpf" placeholder="CPF" class="form-control cpf">
                    </div>
                    <div class="form-group col-md-6">
                        <label>RG</label>
                        <input name="rg" value="<?php echo $updates->RG?>" data-parsley-type="number" placeholder="RG" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-7">
                        <label>Razão Social</label>
                        <input name="razao_social" value="<?php echo $updates->RAZAO_SOCIAL?>"  placeholder="Apenas se for pessoa jurídica" class="form-control">
                    </div>
                    <div class="form-group col-md-5">
                        <label>CNPJ</label>
                        <input name="cnpj" value="<?php echo $updates->CNPJ?>"  placeholder="Apenas se for pessoa jurídica" class="form-control cnpj">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Telefone¹<span class="text-danger">*</span></label>
                        <input name="telefone" value="<?php echo $updates->TELEFONE?>" required="" placeholder="Telefone1" class="form-control phone">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Telefone²</label>
                        <input name="telefone2" value="<?php echo $updates->TELEFONE2?>" type="tel" data-parsley-type="number" placeholder="Telefone²" class="form-control">
                    </div>
                    <div class="form-group col-md-4">
                        <label>CEP</label>
                        <input maxlength="8" value="<?php echo $updates->CEP?>" name="cep" placeholder="CEP" class="form-control cep">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Endereço<span class="text-danger">*</span></label>
                        <input name="endereco" value="<?php echo $updates->ENDERECO?>" required="" placeholder="Endereço" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Cidade<span class="text-danger">*</span></label>
                        <input name="cidade" value="<?php echo $updates->CIDADE?>" required="" placeholder="Cidade" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Bairro</label>
                        <input name="bairro" value="<?php echo $updates->BAIRRO?>" placeholder="Bairro" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail3">Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" value="<?php echo $updates->EMAIL?>" required="" parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="inputEmail3">Senha <span class="text-danger">*</span></label>
                        <input id="hori-pass1" type="password" placeholder="Senha" required="" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="inputEmail3">Confirme a Senha <span class="text-danger">*</span></label>
                        <input data-parsley-equalto="#hori-pass1" <?php echo $updates->SENHA?> type="password" required="" placeholder="Confirme a senha" name="senha" class="form-control" id="hori-pass2">
                    </div>
                </div>
                <div class="form-group row float-right">
                    <button type="submit" class=" btn btn-info waves-effect waves-light"> <i class="fa mdi mdi-content-save m-r-5"></i> <span>Cadastrar</span> </button>
                </div>
            </form>
        </div>
    </div>
</div>