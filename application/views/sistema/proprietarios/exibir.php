<div class="row">
    <div class="card-box table-responsive">
        <div class="col-12">
            <h4 class="m-t-0 header-title">Lista de Proprietarios</h4>
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer" cellspacing="0"
                   width="100%" role="grid" aria-describedby="datatable-buttons_info" style="width: 100%;">
                <thead>
                <tr role="row">
                    <th>IMAGEM</th>
                    <th>#</th>
                    <th>NOME</th>
                    <th>RAZAO SOCIAL</th>
                    <th>BAIRRO</th>
                    <th>CIDADE</th>
                    <th>TELEFONE¹</th>
                    <th>EMAIL</th>
                    <th><i class="dripicons-trash"></i></th>
                    <th><i class="dripicons-pencil"></i></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($proprietarios as $proprietario): ?>
                    <tr role="row" class="odd">
                        <td class="sorting_1"><img class="imagem_aluno" src="<?=  $diretorio = base_url()."assets/sistema/images/imagens_pessoas/$proprietario->CPF" . ".jpg";?> "></td>

                        <th scope="row"><?php echo $proprietario->ID ?></th>
                        <td class="sorting_1"><a href="<?php echo base_url(); ?>index.php/Proprietario/carregarPerfil/<?php echo $proprietario->ID ?>"><?php echo $proprietario->NOME ?></a></th>
                        <td class="sorting_1"><?php echo $proprietario->RAZAO_SOCIAL ?></th>
                        <td class="sorting_1"><?php echo $proprietario->BAIRRO ?></th>
                        <td class="sorting_1"><?php echo $proprietario->CIDADE ?></th>
                        <td class="sorting_1"><?php echo $proprietario->TELEFONE ?></th>
                        <td class="sorting_1"><?php echo $proprietario->EMAIL ?></th>
                        <td>
                            <a href="<?php echo base_url(); ?>index.php/proprietario/delete/<?php echo $proprietario->ID ?> "><i
                                        class="dripicons-trash"></i></a></td>
                        <td>
                            <a href="<?php echo base_url(); ?>index.php/proprietario/update/<?php echo $proprietario->ID ?>"><i
                                        class="dripicons-pencil"></i></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
</div>
</div>

</div>
