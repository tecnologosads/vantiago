<?php
/**
 * Created by PhpStorm.
 * User: Ander
 * Date: 3/26/2019
 * Time: 11:09 PM
 */
?>
<!-- Start Page content -->
<div class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<!-- meta -->
				<div class="profile-user-box card-box bg-custom">
					<div class="row">
						<div class="col-sm-6">
							<span class="pull-left mr-3"><img src="assets/images/users/avatar-1.jpg" alt="" class="thumb-lg rounded-circle"></span>
							<div class="media-body text-white">
								<h4 class="mt-1 mb-1 font-18"><?php echo $perfils->NOME?></h4>
								<p class="font-13 text-light">Proprietário</p>
								<p class="text-light mb-0"><?php echo $perfils->ENDERECO?></p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="text-right">
								<button type="button" class="btn btn-light waves-effect">
									<i class="mdi mdi-account-settings-variant mr-1"></i> Editar perfil
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--/ meta -->
			</div>
		</div>
		<!-- end row -->


		<div class="row">
			<div class="col-md-4">
				<!-- Personal-Information -->
				<div class="card-box">
					<div class="panel-body">
						<div class="text-left">
							<h4 class="header-title mt-0 m-b-20">Dados pessoais</h4>

							<p class="text-muted font-13"><strong>Nome completo :</strong> <span class="m-l-15"><?php echo $perfils->NOME?></span></p>

							<p class="text-muted font-13"><strong>Telefone :</strong><span class="m-l-15"><?php echo $perfils->TELEFONE?></span></p>

							<p class="text-muted font-13"><strong>Nascimento :</strong><span class="m-l-15"><?php echo date(" d/m/Y",strtotime($perfils->DATA_NASCIMENTO))?></span></p>

							<?php  if($perfils->TELEFONE2 != null){?>
								<p class="text-muted font-13"><strong>Telefone² :</strong><span class="m-l-15"><?php echo $perfils->TELEFONE2?></span></p>
							<?php  }?>

							<p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $perfils->EMAIL?></span></p>

							<p class="text-muted font-13"><strong>Endereço :</strong> <span class="m-l-15">
									<?php echo $perfils->ENDERECO . ' | '?>
									<?php if($perfils->BAIRRO != null ){ echo $perfils->BAIRRO . " | "; } ?>
									<?php if($perfils->CIDADE != null ){ echo $perfils->CIDADE; } ?>    </span></p>

							<p class="text-muted font-13"><strong>CEP :</strong> <span class="m-l-15"><?php echo $perfils->CEP?></span></p>

							<p class="text-muted font-13"><strong>Sexo :</strong> <span class="m-l-15"><?php echo $perfils->GENERO?></span></p>
						</div>

						<ul class="social-links list-inline m-t-20 m-b-0">
							<li class="list-inline-item">
								<a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="list-inline-item">
								<a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<!-- Personal-Information -->

<!--				Mensagens-->
<!--				<div class="card-box ribbon-box">-->
<!--					<div class="ribbon ribbon-primary">Messages</div>-->
<!--					<div class="clearfix"></div>-->
<!--					<div class="inbox-widget">-->
<!--						<a href="#">-->
<!--							<div class="inbox-item">-->
<!--								<div class="inbox-item-img"><img src="assets/images/users/avatar-2.jpg" class="rounded-circle" alt=""></div>-->
<!--								<p class="inbox-item-author">Tomaslau</p>-->
<!--								<p class="inbox-item-text">I've finished it! See you so...</p>-->
<!--								<p class="inbox-item-date m-t-10">-->
<!--									<button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success"> Reply </button>-->
<!--								</p>-->
<!--							</div>-->
<!--						</a>-->
<!--					</div>-->
<!--				</div>-->

			</div>


			<div class="col-md-8">

				<div class="row">

					<div class="col-sm-4">
						<div class="card-box tilebox-one">
							<h6 class="text-muted text-uppercase mt-0">Quantidades de vans</h6>
							<h2 class="m-b-20" data-plugin="counterup"><?php echo count($qtd_veiculos); ?></h2>
						</div>
					</div><!-- end col -->
<!---->
<!--					<div class="col-sm-4">-->
<!--						<div class="card-box tilebox-one">-->
<!--							<h6 class="text-muted text-uppercase mt-0">Quantidade de alunos cadastrados </h6>-->
<!--							<h2 class="m-b-20"><span data-plugin="counterup">46,782</span></h2>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="col-sm-4">-->
<!--						<div class="card-box tilebox-one">-->
<!--							<h6 class="text-muted text-uppercase mt-0">Média de classificação</h6>-->
<!--							<h2 class="m-b-20" data-plugin="counterup">1,890</h2>-->
<!--						</div>-->
<!--					</div>-->

				</div>
				<!-- end row -->

<!--				Corpo da página-->
<!--				<div class="card-box">-->
<!--					<h4 class="header-title mt-0 mb-3">Experience</h4>-->
<!--					<div class="">-->
<!--						<div class="">-->
<!--							<h5 class="text-custom m-b-5">Lead designer / Developer</h5>-->
<!--							<p class="m-b-0">websitename.com</p>-->
<!--							<p><b>2010-2015</b></p>-->
<!---->
<!--							<p class="text-muted font-13 m-b-0">Lorem Ipsum is simply dummy text-->
<!--								of the printing and typesetting industry. Lorem Ipsum has-->
<!--								been the industry's standard dummy text ever since the-->
<!--								1500s, when an unknown printer took a galley of type and-->
<!--								scrambled it to make a type specimen book.-->
<!--							</p>-->
<!--						</div>-->
<!---->
<!--						<hr>-->
<!---->
<!--						<div class="">-->
<!--							<h5 class="text-custom m-b-5">Senior Graphic Designer</h5>-->
<!--							<p class="m-b-0">coderthemes.com</p>-->
<!--							<p><b>2007-2009</b></p>-->
<!---->
<!--							<p class="text-muted font-13">Lorem Ipsum is simply dummy text-->
<!--								of the printing and typesetting industry. Lorem Ipsum has-->
<!--								been the industry's standard dummy text ever since the-->
<!--								1500s, when an unknown printer took a galley of type and-->
<!--								scrambled it to make a type specimen book.-->
<!--							</p>-->
<!--						</div>-->
<!---->
<!--					</div>-->
<!--				</div>-->

				<div class="card-box">
					<h4 class="header-title mb-3">Vans</h4>

					<div class="table-responsive">
						<table class="table m-b-0">
							<thead>
							<tr>
								<th>Placa da van</th>
								<th>Ano</th>
								<th>Fabricante</th>
								<th>Qtd de lugares</th>
								<th>Vagas</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>Adminox Admin</td>
								<td>01/01/2015</td>
								<td>07/05/2015</td>
								<td><span class="label label-info">Work in Progress</span></td>
								<td>Coderthemes</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			<!-- end col -->

		</div>
		<!-- end row -->


	</div> <!-- container -->

</div> <!-- content -->

