<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>index.php/Passageiro" class="btn btn-primary"><i class=" mdi mdi-keyboard-backspace" ></i>   Voltar</a>
		<div class="card-box">
			<h4 class="m-t-0 header-title">Atualização das informações do(a): <?php echo $update->NOME ?></h4>
			<p class="text-muted m-b-30 font-13">
				Dados obrigátorios <span class="text-danger">*</span>
			</p>

			<form method="post" action="<?php echo base_url();?>Index.php/Passageiro/updateset">
				<input name="id" type="hidden" value="<?php echo $update->ID?>">
				<div class="form-row">
					<!--					nome-->
					<div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Nome<span class="text-danger">*</span></label>
						<input type="text" required name="NOME" value="<?php echo $update->NOME;?>" class="form-control" id="inputEmail4" placeholder="Nome">
					</div>

					<!--					Idade-->
					<div class="form-group col-md-1">
						<label for="inputEmail4" class="col-form-label">Idade<span class="text-danger">*</span></label>
						<select name="IDADE" class="form-control">
							<option value="<?php echo $update->IDADE?>"><?php echo $update->IDADE?></option>
							<?php for($i=15; $i<100; $i++){ ?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php } ?>
						</select>
					</div>

					<!--					Turno-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Turno<span class="text-danger">*</span></label>
						<select name="TURNO" class="form-control">
							<?php if($update->TURNO==='N'){?>
								<option value="N">Noturno</option>
								<option value="V">Vespertino</option>
								<option value="M">Matutino</option>

							<?php } elseif ($TURNO->turno==='V'){?>
								<option value="V">Vespertino</option>
								<option value="N">Noturno</option>
								<option value="M">Matutino</option>

							<?php } elseif ($TURNO->turno==='M'){?>
								<option value="M">Matutino</option>
								<option value="V">Vespertino</option>
								<option value="N">Noturno</option>
							<?php }else { ?>
								<option value="M">Matutino</option>
								<option value="V">Vespertino</option>
								<option value="N">Noturno</option>
							<?php } ?>
						</select>
					</div>
				</div>
				<!--				Endereço-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Endereço<span class="text-danger">*</span></label>
					<input name="ENDERECO" required value="<?php $update->ENDERECO; ?>" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
				</div>

				<div class="form-row">
					<!--				Cidade-->
					<div class="form-group col-md-6">
						<label for="inputCity" class="col-form-label">Cidade</label>
						<input name='CIDADE' value="<?php $update->CIDADE?>" type="text" class="form-control" id="inputCity">
					</div>

					<!--					Estado-->
					<div class="form-group col-md-4">
						<label for="inputState" class="col-form-label">Estado</label>
						<select name="ESTADO" id="inputState" class="form-control">
							<?php foreach ($estados as $estado) {?>
								<option value="<?php echo $estado->nome?>"><?php echo $estado->nome?></option>
							<?php  }?>
						</select>
					</div>
				</div>

				<button type="submit" class="btn btn-primary">Atualizar</button>
			</form>
		</div>
	</div>
</div>
