<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>index.php/Passageiro" class="btn btn-primary"><i class=" mdi mdi-keyboard-backspace" ></i>   Voltar</a>
		<div class="card-box">
			<h4 class="m-t-0 header-title">Atualização do passageiro <?php  ?></h4>
			<p class="text-muted m-b-30 font-13">
				Dados obrigátorios <span class="text-danger">*</span>
			</p>

			<form method="post" action="<?php echo base_url();?>Index.php/Passageiro/create">
				<div class="form-row">
<!--					nome-->
					<div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Nome<span class="text-danger">*</span></label>
						<input required type="text" minlength="4" name="NOME" class="form-control" id="inputEmail4" placeholder="Nome">
					</div>

<!--					Idade-->
					<div class="form-group col-md-1">
						<label for="inputEmail4" class="col-form-label">Idade<span class="text-danger">*</span></label>
						<select required name="IDADE" class="form-control">
							<option></option>
							<?php for($i=15; $i<100; $i++){ ?>
								<option value="<?php echo $i?>"><?php echo $i?></option>
							<?php } ?>
						</select>
					</div>

<!--					Turno-->
					<div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Turno<span class="text-danger">*</span></label>
						<select required name="TURNO" class="form-control">
							<option></option>
							<option value="M">Matutino</option>
							<option value="V">Vespertino</option>
							<option value="N">Noturno</option>
						</select>
					</div>

				</div>
<!--				Endereço-->
				<div class="form-group">
					<label for="inputAddress" class="col-form-label">Endereço<span class="text-danger">*</span></label>
					<input minlength="20" required name="ENDERECO" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
				</div>

<!--				localização-->
				<div class="form-row">
					<!--				Cidade-->
					<div class="form-group col-md-6">
						<label for="inputCity" class="col-form-label">Cidade</label>
						<input name='CIDADE' type="text" class="form-control" id="inputCity">
					</div>
<!--					Estado-->

					<div class="form-group col-md-4">
						<label for="inputState" class="col-form-label">Estado</label>
						<select name="ESTADO" id="inputState" class="form-control">
							<?php foreach ($cidades as $cidade) {?>
								<option value="<?php echo $cidade->nome?>"><?php echo $cidade->nome?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<button type="submit" class="btn btn-primary"><i class="mdi mdi-check"></i>   Cadastrar</button>
				<button type="reset" id="tooltip-animation" class="btn btn-primary" title="Atenção, isso vai apagar todos os campos preenchidos"><i class=" mdi mdi-delete-empty "></i> Limpar o formulário</button>
			</form>
		</div>
	</div>
</div>
