<a href="<?php echo base_url(); ?>index.php/Passageiro/carregarCadastro"  type="button" class="btn btn-primary waves-light waves-effect">Novo <i class="mdi mdi-account-plus"></i></a>
<div class="row">
                <div class="col-lg-10">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title">Lista de passageiros</h4>
                        <table class="table">
                            <thead>
                            <tr>
								<th>Nome</th>
								<th>Telefone</th>
								<th>Turno</th>
								<th>idade</th>
								<th>Endereço</th>
								<th><i class="dripicons-trash"></i></th>
								<th><i class="dripicons-pencil"></i></th>
							</tr>
                            </thead>
                            <tbody>
                            <?php foreach ($passageiros as $passageiro): ?>
                                <tr>
									<th scope="row"><a href="<?php echo base_url(); ?>index.php/Passageiro/carregarPerfil/<?php echo $passageiro->ID ?>"><?php echo $passageiro->NOME?></a></th>
                                    <th scope="row"><?php echo $passageiro->TELEFONE?></th>
                                    <?php if ($passageiro->TURNO==='M'){ ?>
										<th scope="row"><?php echo $passageiro->TURNO?> - Matutino</th>

									<?php } elseif ($passageiro->TURNO==='N'){?>
										<th scope="row"><?php echo $passageiro->TURNO?> - Noturno</th>

									<?php } elseif ($passageiro->TURNO==='V'){?>
										<th scope="row"><?php echo $passageiro->TURNO?> - Vespertino</th>

									<?php } ?>
                                    <th scope="row"><?php echo $passageiro->IDADE?></th>
                                    <th scope="row"><?php echo $passageiro->ENDERECO?></th>
									<td><a href="#custom-modal" class="btn btn-light waves-effect w-md mr-2 mb-2" data-animation="flash" data-plugin="custommodal"
										   data-overlaySpeed="100" data-overlayColor="#36404a"><i class="dripicons-trash"></i></a></td>
<!--									<td><a href="--><?php //echo base_url(); ?><!--index.php/Passageiro/delete/--><?php //echo $passageiro->id ?><!-- "><i-->
<!--												class="dripicons-trash"></i></a></td>-->
									<td><a href="<?php echo base_url(); ?>index.php/Passageiro/updateget/<?php echo $passageiro->ID ?>"><i
												class="dripicons-pencil"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>

</div>
