<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Perfil do <?php echo $perfils->NOME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <?php $this->load->helper("csssistema_helper") ?>
    <?php $this->load->helper("jssistematop_helper") ?>
</head>
<body>
<?php $this->load->view("sistema/htmlelements/verticalmenu"); ?>
<div id="wrapper">
    <div class="content-page">
        <div class="card-box">
            <?php $this->load->view("sistema/passageiro/ferramentas/perfil"); ?>
        </div>
    </div>
</div>
<?php $this->load->helper("jssistemadown_helper") ?>
</body>
</html>
