<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php $this->load->helper("csssistema_helper") ?>
    <?php $this->load->helper("jssistematop_helper") ?>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin="">
    </script>
    <style type="text/css">

        #mapid {
            height: 500px;
        }
    </style>
</head>
<body>


<?php $this->load->view("sistema/htmlelements/header_home") ?>
<div>
    <div>
        <div class="card-box">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="profile-user-box card-box bg-custom">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="media-body text-white">
                                           <img class="col-md-3" src="<?= base_url() ?>assets/sistema/images/imagens_veiculos/<?= $perfils->PLACA . '.jpg'?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card-box">
                                <div class="panel-body">
                                    <h4 class="header-title mt-0 m-b-20">Descrição do veiculo</h4>
                                    <p class="text-muted font-13">
                                        <?php echo $perfils->DESCRICAO_VEICULO ?>
                                    </p>

                                    <hr/>

                                    <div class="text-left">
                                        <h4 class="header-title mt-0 m-b-20">DADOS DO VEÍCULO</h4>

                                        <p class="text-muted font-13"><strong>Modelo: </strong> <span class="m-l-15"><?php echo $perfils->MODELO ?></span></p>
                                        <p class="text-muted font-13"><strong>Fabricante: </strong><span class="m-l-15"><?php echo $perfils->FABRICANTE ?></span></p>
                                        <p class="text-muted font-13"><strong>Quantidade de lugares: </strong><span class="m-l-15"><?php echo $perfils->QTD_LUGARES ?></span></p>
                                        <p class="text-muted font-13"><strong>Tipo do Veículo: </strong><span class="m-l-15"><?php echo $perfils->TIPO ?></span></p>
                                        <p class="text-muted font-13"><strong>Placa: </strong><span class="m-l-15"><?php echo $perfils->PLACA ?></span></p>
                                    </div>
                                    <hr/>
                                    <div class="text-left">
                                        <h4 class="header-title mt-0 m-b-20">DADOS DO PROPRIETÁRIO</h4>

                                        <p class="text-muted font-13"><strong>Proprietario: </strong><span class="m-l-15"><?php echo $perfils->NOME ?></span></p>
                                        <p class="text-muted font-13"><strong>Email: </strong><span class="m-l-15"><?php echo $perfils->EMAIL ?></span></p>
                                        <p class="text-muted text-success font-15"><strong>Telefone: <span class="m-l-15"><?php echo $perfils->TELEFONE ?></span></strong></p>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-md-8">
                            <div class="card-box">
                                <h4 class="header-title mt-0 mb-3">Regiões atendidas pela Van</h4>
                                <div class="">
                                    <div id="mapid" class="col-md-12"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>

    var mymap = L.map('mapid').setView([-16.149762, -47.979578], 11);

    <?php foreach($exibirLatitudeLongitude as $ll): ?>

    L.marker([<?= $ll->LATITUDE ?>, <?= $ll->LONGITUDE ?>]).addTo(mymap).bindPopup("<b><?= $ll->BAIRRO ?></b>").openPopup();

    <?php endforeach; ?>




    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

</script>
<?php $this->load->helper("jssistemadown_helper") ?>
</body>
</html>








