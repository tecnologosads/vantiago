<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Buscar Vans</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php $this->load->helper("csssistema_helper") ?>
    <?php $this->load->helper("jssistematop_helper") ?>
</head>
<body>
<?php $this->load->view("sistema/htmlelements/header_home") ?>

<section class="row pesquisa_home">
    <div class="col section2">
    </div>
</section>

<section class="row cx_detalhes">
    <h3 class="col-12">BUSCAR VANS</h3>

    <?php foreach ($listarVans as $lv): ?>
        <div class="col-3 destaques">
            <a href="<?= base_url() ?>index.php/controleexterno/exibirUmaVan/<?= $lv->ID_VEICULO ?>">
                <div class="veiculos_destaque">
                    <div>
                        <img class="img_van-destaque"
                             src="<?= base_url() ?>assets/sistema/images/imagens_veiculos/<?php echo $lv->PLACA ?>.jpg">
                        <h5><?= $lv->NOME ?></h5>
                        <div class="lugares_ano">
                            <span class="lugares"><?= $lv->QTD_LUGARES ?> LUGARES</span>
                            <span class="ano_veiculo"><?= $lv->ANO ?></span>
                        </div>
                        <span class="detalhes"> + Detalhes</span>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>




</section>


</body>

</html>