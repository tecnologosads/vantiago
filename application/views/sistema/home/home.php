<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Univan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <?php $this->load->helper("csssistema_helper") ?>
    <?php $this->load->helper("jssistematop_helper") ?>


</head>
<body>

<?php $this->load->view("sistema/htmlelements/header_home") ?>

<section class="row pesquisa_home">
    <div class="col section2">

        <div class="form-row col-5 formulario justify-content-center">
            <form role="form" method="post" action="<?php base_url() ?>index.php/controleexterno/buscarVans/">
                <h3>BUSCA DE VANS ESCOLARES</h3>
                <div class="form-group col-12">
                    <label class="col-form-label">Embarque</label>
                    <select class="form-control" id="id_categoria">
                        <option>Escolha uma Cidade</option>

                        <?php foreach ($cidades as $c):
                            echo "<option class='option_cidade' value='$c->ID'> $c->CIDADE </option>";
                        endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-group col-12">
                    <select name="idBairro" class="form-control" id="id_sub_categoria">
                    </select>
                </div>

                <div class="linha col-12"></div>

                <div class="form-group col-12">
                    <label class="col-form-label">Destino</label>
                    <select class="form-control">
                        <option>UNIDESC</option>
                    </select>
                </div>
                <div class="form-group col-12">
                    <label class="col-form-label">Turno</label>
                    <select name="idTurno" class="form-control">
                        <option value="2">NOTURNO</option>
                        <option value="1">MATUTINO</option>
                    </select>
                </div>

                <div class="form-group col-12">
                    <button type="submit" class="btn btn-warning col-12">PESQUISAR</button>
                </div>
            </form>
        </div>
        <div>
            <img src="<?= base_url() ?>assets/sistema/images/home/van_recortada.jpg">
        </div>
    </div>
</section>

<section class="row cx_detalhes">
    <h3 class="col-12">VANS EM DESTAQUE</h3>
    <?php foreach ($vanDestaque as $vd): ?>
        <div class="col-3 destaques">
            <a href="<?= base_url() ?>index.php/controleexterno/exibirUmaVan/<?= $vd->ID_VEICULO ?>">
                <div class="veiculos_destaque">
                    <div>
                        <img class="img_van-destaque"
                             src="<?= base_url() ?>assets/sistema/images/imagens_veiculos/<?php echo $vd->PLACA ?>.jpg">
                        <h5><?= $vd->NOME ?></h5>
                        <div class="lugares_ano">
                            <span class="lugares"><?= $vd->QTD_LUGARES ?> LUGARES</span>
                            <span class="ano_veiculo"><?= $vd->ANO ?></span>
                        </div>
                        <span class="detalhes"> + Detalhes</span>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</section>


</body>

</html>
<?php // $this->load->helper("jssistemadown_helper") ?>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#id_categoria').change(function () {
            var valorCidade = $(this).val();
            var url = "<?php  base_url() ?>index.php/veiculo/gerarJsonCidades/" + valorCidade;

            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var options = '<option>Escolha o Bairro</option>';

                    for (var i = 0; i < data.length; i++) {
                        options += '<option value="' + data[i].id + '">' + data[i].nome_sub_categoria + '</option>';
                    }
                    $('#id_sub_categoria').html(options).show();
                },
            });
        });
    });
</script>

