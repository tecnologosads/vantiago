<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.slimscroll.js"></script>

<!-- Flot chart -->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.time.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.pie.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.crosshair.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/curvedLines.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.flot.axislabels.js"></script>


<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.slimscroll.js"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="<?php echo base_url(); ?>assets/sistema/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/buttons.print.min.js"></script>

<!-- Key Tables -->
<script src="<?php echo base_url(); ?>assets/sistema/js/dataTables.keyTable.min.js"></script>

<!-- Responsive examples -->
<script src="<?php echo base_url(); ?>assets/sistema/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/responsive.bootstrap4.min.js"></script>

<!-- Selection table -->
<script src="<?php echo base_url(); ?>assets/sistema/js/dataTables.select.min.js"></script>

<!-- KNOB JS -->
<!--+[if IE]>
<script src="assets/sistema/js/excanvas.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.knob.js"></script>

<!-- Dashboard Init -->
<!--<script src="--><?php //echo base_url();?><!--assets/sistema/pages/jquery.dashboard.init.js"></script>-->

<!-- Sweet Alert Js  -->
<script src="<?php echo base_url(); ?>assets/sistema/js/sweetalert2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/pages/jquery.sweet-alert.init.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.sweet-alert.init.js"></script>

<!-- Modal-Effect contacts-->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/custombox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/legacy.min.js"></script>

<!-- Modal-Effect -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/custombox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/legacy.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/sistema/js/fim_tabelas.js"></script>

<script src="<?php echo base_url();?>assets/sistema/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.slimscroll.js"></script>



<!-- App js -->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.core.js"></script>
<!--<script src="--><?php //echo base_url();?><!--assets/sistema/js/jquery.app.js"></script>-->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.mask.js"></script>

<!--form mask-->
<script src="<?php echo base_url();?>assets/sistema/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/sistema/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/sistema/js/parsley.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
        $('.cep').mask('00000-000');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.phone').mask('(00) 0 0000-0000');
        $('.placa').mask('AAA-0000');
        $('.inteiro11').mask('00000000000');
        $('.inteiro4').mask('0000');
        $('.cnh').mask('00000000000');
    });
</script>

<script type="text/javascript">
	jQuery(function($) {
		$('.autonumber').autoNumeric('init');
	});
	jQuery.browser = {};
	(function () {
		jQuery.browser.msie = false;
		jQuery.browser.version = 0;
		if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
			jQuery.browser.msie = true;
			jQuery.browser.version = RegExp.$1;
		}
	})();
</script>

<!-- Toastr js -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/jquery-toastr/jquery.toast.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/sistema/pages/jquery.toastr.js" type="text/javascript"></script>

<!-- Tooltipster js -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/tooltipster/tooltipster.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/pages/jquery.tooltipster.js"></script>

<!-- Sweet Alert Js  -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/pages/jquery.sweet-alert.init.js"></script>

<!-- Modal-Effect -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/custombox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/custombox/js/legacy.min.js"></script>

<!-- Counter Up  -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/counterup/jquery.counterup.min.js"></script>

<!--Dashboard-->
<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.slimscroll.js"></script>

<!-- Flot chart -->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.crosshair.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/curvedLines.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/plugins/flot-chart/jquery.flot.axislabels.js"></script>

<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="assets/sistema/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/sistema/plugins/jquery-knob/jquery.knob.js"></script>

<!-- Dashboard Init -->
<script src="<?php echo base_url(); ?>assets/sistema/pages/jquery.dashboard.init.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/sistema/js/jquery.app.js"></script>

<script src="<?php echo base_url();?>assets/sistema/js/js_cidades.js"></script>

<!--Calendar-->
<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.slimscroll.js"></script>
<!-- Jquery-Ui -->
<script src="<?php echo base_url();?>assets/sistema/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- SCRIPTS -->
<script src="<?php echo base_url();?>assets/sistema/plugins/moment/moment.js"></script>
<script src='<?php echo base_url();?>assets/sistema/plugins/fullcalendar/js/fullcalendar.min.js'></script>
<script src="<?php echo base_url();?>assets/sistema/pages/jquery.calendar.js"></script>
<!-- App js -->
<script src="<?php echo base_url();?>assets/sistema/js/jquery.core.js"></script>
<script src="<?php echo base_url();?>assets/sistema/js/jquery.app.js"></script>
